#simpler version for people who don't have foray
#they still need to have pyexpander installed, though.
#there is no support for parallel fortran support yet
#this has do be done manually right now

#determine which compiler to use and for which mode (debug or release)
ifeq ($(FC),gfortran)
  ifeq ($(mode),release)
    DEBUG := -O3 -m64
  else
    DEBUG := -g -m64 -fbounds-check -fbacktrace \
             -ffpe-trap=zero,overflow,underflow \
             -pedantic -Wall -Wextra -Wconversion
  endif
  FFORM :=  -ffree-form -ffree-line-length-none
else
  FC := ifort
  ifeq ($(mode),release)
    DEBUG := -fast
  else
    DEBUG := -O0 -fpe0 -g -traceback -check all \
             -debug all -ftrapuv -gen-interfaces \
             -warn interfaces
  endif
  FFORM :=
endif

ifeq ($(mode),release)
  mode := release
else
  mode := debug
endif

#set the compiler options for the output directory
ifeq ($(FC),gfortran)
  FC_OPT := -J
else
  FC_OPT := -module
endif


src_dir := src
build_dir := build/make/$(FC)/$(mode)
bin_dir := bin/make/$(FC)/$(mode)


objs := cdata.o cerror.o \
        math.o lsd_core.o \
        lsd4_data.o lsd4_io.o

mods := cdata.mod cerror.mod \
        math.mod lsd_core.mod \
        lsd4_data.mod lsd4_io.mod

objs := $(addprefix $(build_dir)/,$(objs))
mods := $(addprefix $(build_dir)/,$(mods))


all: build

build: | $(build_dir) $(bin_dir)

$(build_dir):
	mkdir -p $(build_dir)

$(bin_dir):
	mkdir -p $(bin_dir)

build: $(bin_dir)/lsd4 $(bin_dir)/lsd4s

#Individual dependencies
$(bin_dir)/lsd4: $(objs) $(build_dir)/lsd4.o
	$(FC) $(DEBUG) $^ -o $@
	if [ ! -f "Tests/lsd4.$(mode).$(FC)" ]; then ln -s ../$(bin_dir)/lsd4 Tests/lsd4.$(mode).$(FC); fi;

$(bin_dir)/lsd4s: $(objs) $(build_dir)/lsd_simple.o $(build_dir)/lsd4s.o
	$(FC) $(DEBUG) $^ -o $@
	if [ ! -f "Tests/lsd4s.$(mode).$(FC)" ]; then ln -s ../$(bin_dir)/lsd4s Tests/lsd4s.$(mode).$(FC); fi;

$(build_dir)/cdata.o: $(build_dir)/cdata.f90
	$(FC) -c $(DEBUG) $(FFORM) $< $(FC_OPT) $(build_dir) -o $@

$(build_dir)/cdata.f90: $(src_dir)/cdata.F90
	expander.py --eval="pylsd=False" -f $(src_dir)/cdata.F90 > $(build_dir)/cdata.f90

$(build_dir)/cerror.o: $(build_dir)/cerror.f90
	$(FC) -c $(DEBUG) $(FFORM) $< $(FC_OPT) $(build_dir) -o $@

$(build_dir)/cerror.f90: $(src_dir)/cerror.F90 $(src_dir)/cerror_data.txt
	expander.py --eval="pylsd=False;cerror_datafile='$(src_dir)/cerror_data.txt'" -f $(src_dir)/cerror.F90 > $(build_dir)/cerror.f90

$(build_dir)/math.o: $(src_dir)/math.f
	$(FC) -c $(DEBUG) $< $(FC_OPT) $(build_dir) -o $@

$(build_dir)/lsd4_data.o: $(src_dir)/lsd4_data.f90
	$(FC) -c $(DEBUG) $(FFORM) $< $(FC_OPT) $(build_dir) -o $@

$(build_dir)/lsd4_io.o: $(src_dir)/lsd4_io.f90
	$(FC) -c $(DEBUG) $(FFORM) $< $(FC_OPT) $(build_dir) -o $@

$(build_dir)/lsd_core.o: $(src_dir)/lsd_core.f90
	$(FC) -c $(DEBUG) $(FFORM) $< $(FC_OPT) $(build_dir) -o $@

$(build_dir)/lsd4.o: lsd4/lsd4.f90
	$(FC) -c $(DEBUG) $(FFORM) $< $(FC_OPT) $(build_dir) -o $@

$(build_dir)/lsd4s.o: lsd4s/lsd4s.f90
	$(FC) -c $(DEBUG) $(FFORM) $< $(FC_OPT) $(build_dir) -o $@

$(build_dir)/lsd_simple.o: $(src_dir)/lsd_simple.f90
	$(FC) -c $(DEBUG) $(FFORM) $< $(FC_OPT) $(build_dir) -o $@


.PHONY: clean

clean:
	@rm -f $(build_dir)/* $(bin_dir)/*
