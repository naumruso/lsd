!------------------------------------------------------------------------------
! Uppsala University, Department of Physics and Astronomy, 
! Least-Squares Deconvolution Code
!------------------------------------------------------------------------------
!> @file lsd4s.f90
!> @author NRusomarov
!
!> @brief Standalone program that implements lsd_simple.
!
!> @details To be written...
!
!> @todo Write better documentation.
!------------------------------------------------------------------------------
PROGRAM LSD4S
  USE, INTRINSIC :: ISO_FORTRAN_ENV
  USE CDATA, ONLY: I4B, DP, IOMSG_LEN
  USE CERROR
  USE LSD4_IO
  USE LSD_CORE, only: SET_VSPACE_LSD
  USE LSD_SIMPLE, only: RUN_LSD
  IMPLICIT NONE

  !constant program params
  LOGICAL :: LSCL = .TRUE.    !rescale profiles to sqrt(chi)
  LOGICAL :: PREINIT = .TRUE. !run preinit_lsd
  LOGICAL :: VERBOSE = .TRUE. !print additional data to std out
  LOGICAL :: DEBUG = .TRUE.   !print error info to std err

  !array sizes
  INTEGER(kind=i4b) :: LINSIZ !spectral lines
  INTEGER(kind=i4b) :: LSDSIZ !LSD profiles
  INTEGER(kind=i4b) :: SPSIZ  !points in spectrum
  INTEGER(kind=i4b) :: VSIZ   !points in the LSD profile

  !data arrays
  real(kind=dp), allocatable :: WLCLIN(:)
  real(kind=dp), allocatable :: WGTLIN(:,:)
  real(kind=dp), allocatable :: WL(:)
  real(kind=dp), allocatable :: SP(:)
  real(kind=dp), allocatable :: SIGSP(:)

  !these arrays are allocated in run_lsd
  REAL(kind=dp), allocatable :: PLSD(:) !LSD profiles
  REAL(kind=dp), allocatable :: SIGLSD(:) !sigma for LSD profiles
  real(kind=dp), allocatable :: VLSD(:) !velocity space
  real(kind=dp), allocatable :: MSIG(:) !line weights
  real(kind=dp), allocatable :: MSP(:) !modelled spectrum

  real(kind=dp) :: VFIRST
  real(kind=dp) :: VLAST
  real(kind=dp) :: VSTEP
  real(kind=dp) :: REG
  integer(kind=i4b) :: POL
  real(kind=dp) :: CHI
  integer(kind=i4b) :: NWL

  !TMP VARS
  INTEGER(KIND=I4B) :: I
  INTEGER(KIND=I4B) :: IERR, ISTAT
  INTEGER(KIND=I4B) :: FIUNIT, LSD_FOUNIT, MOD_FOUNIT, WLN_FOUNIT
  CHARACTER(LEN=IOMSG_LEN) :: FINAME, FONAME
  CHARACTER(LEN=IOMSG_LEN) :: PREFIX !IO PREFIX (BASE NAME FOR OUTPUT)

  !INIT ERROR CODES
  CALL CERROR_INIT()

  !OPEN CONFIGURATION FILE, PROCESS IO ERRORS
  DO WHILE (.TRUE.)
    CALL GET_COMMAND_ARGUMENT(1,FINAME) !GET FIRST ARGUMENT FROM COMMAND LINE
    IF(LEN(TRIM(FINAME)).EQ.0) THEN !NOTHING ENTERED
      WRITE(OUTPUT_UNIT,*) '#ENTER INPUT FILENAME: '
      READ(INPUT_UNIT,'(A)', IOSTAT=IERR) FINAME
      IF (IERR.NE.0) THEN
        if (debug) WRITE(ERROR_UNIT, *) TRIM(CERROR_MSG(CERROR_io_BAD_INPUT))
        STOP 1
      ENDIF
    END IF

    OPEN(NEWUNIT=FIUNIT,FILE=TRIM(FINAME),STATUS='OLD',IOSTAT=IERR)
    IF(IERR.NE.0) THEN
      if (debug) WRITE(ERROR_UNIT,*) TRIM(CERROR_MSG(CERROR_io_OPENFILE))//':'//trim(finame)
      STOP 1
    END IF
    EXIT
  ENDDO

  !DETERMINE PREFIX FOR OUTPUT FILES
  FINAME=COMPRFUNC(FINAME)
  I=INDEX(FINAME,'.')
  PREFIX=FINAME(1:I-1)

  !RETURNS THE SIZES OF ALL ARRAYS.
  CALL GET_DIMS_LSD(FIUNIT, LINSIZ, LSDSIZ, SPSIZ, VSIZ, ierr, debug)
  if (ierr .ne. 0) then
    if (debug) write(ERROR_UNIT, *) 'LSD4S:GET_DIMS_LSD:'//TRIM(CERROR_MSG(IERR))
    stop 1
  endif
  IF (VERBOSE) THEN
    WRITE(OUTPUT_UNIT, '(A,I6)') 'LINSIZ : ',LINSIZ
    WRITE(OUTPUT_UNIT, '(A,I6)') 'LSDSIZ : ',LSDSIZ
    WRITE(OUTPUT_UNIT, '(A,I6)') 'SPSIZ  : ',SPSIZ
    WRITE(OUTPUT_UNIT, '(A,I6)') 'VSIZ   : ',VSIZ
  ENDIF

  !ALLOCATE THE ARRAYS FOR OUR INPUT DATA
  CALL INIT_WORKSPACE
  if (ierr .ne. 0) then
    if (debug) write(ERROR_UNIT, *) 'LSD4S:INIT_WORKSPACE:'//TRIM(CERROR_MSG(IERR))
    stop 1
  endif

  IF (VERBOSE) THEN
    WRITE(OUTPUT_UNIT, *) '#WORKSPACE ARRAYS ALLOCATED SUCCESFULLY.'
    WRITE(OUTPUT_UNIT, '(A,I6,A)') 'WLCLIN : (',LINSIZ,')'
    WRITE(OUTPUT_UNIT, '(A,I6,A,I6,A)') 'WGTLIN : (',LSDSIZ,',',LINSIZ,')'
    WRITE(OUTPUT_UNIT, '(A,I6,A)') 'WL     : (',SPSIZ,')'
    WRITE(OUTPUT_UNIT, '(A,I6,A)') 'SP     : (',SPSIZ,')'
    WRITE(OUTPUT_UNIT, '(A,I6,A)') 'SIGSP  : (',SPSIZ,')'
    WRITE(OUTPUT_UNIT, '(A,I6,A)') 'VLSD   : (',VSIZ,')'
  ENDIF

  !READ INPUT DATA AND COPY DATA TO ARRAYS (NEW VERSION OF THE INTERFACE)
  CALL INPUT_LSD(FIUNIT,&
                &LINSIZ, LSDSIZ, SPSIZ,&
                &WLCLIN, WGTLIN, WL, SP, SIGSP,&
                &REG, POL,&
                &VFIRST, VLAST, VSTEP, ierr, debug)
  if (ierr .ne. 0) then
    if (debug) write(ERROR_UNIT, *) 'LSD4S:INPUT_LSD:'//TRIM(CERROR_MSG(IERR))
    stop 1
  endif

  IF (VERBOSE) THEN
    !OUTPUT INFO ABOUT REGULARIZATION
    IF(REG .LT. 0_DP) THEN
      WRITE(OUTPUT_UNIT,*) "DEFAULT REGULARIZATION"
    ELSE
      WRITE(OUTPUT_UNIT,'(A,E12.2)') 'REG    :',REG
    END IF

    !SUBTRACT OBSERVED SPECTRUM FROM ONE IF WE ARE DEALING WITH INTENSITY SPECTRUM
    IF(POL.EQ.1) THEN
      WRITE(OUTPUT_UNIT,*) "#OBSERVATIONS ARE INTERPRETED AS POLARIZATION"
    ELSE
      WRITE(OUTPUT_UNIT,*) "#OBSERVATIONS ARE INTERPRETED AS INTENSITY"
      SP=1_DP-SP
    END IF
  ENDIF

  !CALCULATE THE VELOCITY GRID
  CALL SET_VSPACE_LSD(VFIRST, VLAST, VSTEP, VSIZ, VLSD)
  IF (VERBOSE) THEN
    WRITE(OUTPUT_UNIT,'(A,E12.5)') 'VFIRST :',VFIRST
    WRITE(OUTPUT_UNIT,'(A,E12.5)') 'VLAST  :',VLAST
    WRITE(OUTPUT_UNIT,'(A,E12.5)') 'VSTEP  :',VSTEP
  ENDIF

  !ONLY ONE CALL TO RUN_LSD TO CALCULATE THE LSD PROFILES
  CALL RUN_LSD(WL, SP, SIGSP, WLCLIN, WGTLIN, VLSD,&
               &REG, LSCL, PREINIT,&
               &PLSD, SIGLSD,&
               &MSIG=MSIG, MSP=MSP, CHI=CHI, NWL=NWL,&
               &IERR=IERR, DEBUG=DEBUG)
  if (ierr .ne. 0) then
    if (debug) WRITE(ERROR_UNIT, *) 'LSD4S:RUN_LSD:'//TRIM(CERROR_MSG(IERR))
    STOP 1
  endif

  !OPEN UNITS FOR OUTPUT
  !LSD PROFILES
  FONAME=TRIM(PREFIX)//'.lsd'
  OPEN(NEWUNIT=LSD_FOUNIT,FILE=TRIM(FONAME),STATUS='REPLACE',FORM='FORMATTED', IOSTAT=IERR)
  IF (IERR .NE. 0) THEN
    if (debug) WRITE(ERROR_UNIT, *) 'LSD4S:'//TRIM(CERROR_MSG(CERROR_io_OPENFILE))//':'//trim(foname)
    STOP 1
  ENDIF

  !MODEL SPECTRUM
  FONAME=TRIM(PREFIX)//'.mod'
  OPEN(NEWUNIT=MOD_FOUNIT,FILE=TRIM(FONAME),STATUS='REPLACE',FORM='FORMATTED',IOSTAT=IERR)
  IF (IERR .NE. 0) THEN
    if (debug) WRITE(ERROR_UNIT, *) 'LSD4S:'//TRIM(CERROR_MSG(CERROR_io_OPENFILE))//':'//trim(foname)
    STOP 1
  ENDIF
  
  !WEIGHT FOR EACH LINE AS USED IN THE LINE-PATTERN MATRIX
  FONAME=TRIM(PREFIX)//'.wln'
  OPEN(NEWUNIT=WLN_FOUNIT,FILE=TRIM(FONAME),STATUS='REPLACE',FORM='FORMATTED',IOSTAT=IERR)
  IF (IERR .NE. 0) THEN
    if (debug) WRITE(ERROR_UNIT, *) 'LSD4S:'//TRIM(CERROR_MSG(CERROR_io_OPENFILE))//':'//trim(foname)
    STOP 1
  ENDIF

  !OUTPUT RESULTS
  CALL OUTPUT_LSD(LSD_FOUNIT, MOD_FOUNIT, WLN_FOUNIT,&
                 &VSIZ, LSDSIZ, SPSIZ, LINSIZ, NWL,&
                 &WL, MSP, SP, SIGSP,&
                 &WLCLIN, MSIG,&
                 &VLSD, PLSD, SIGLSD,&
                 &CHI, POL)

  !CLOSE UNITS
  CLOSE(UNIT=LSD_FOUNIT)
  CLOSE(UNIT=MOD_FOUNIT)
  CLOSE(UNIT=WLN_FOUNIT)

  !FINISH GRACEFULLY
  CALL DEINIT_WORKSPACE
  if (ierr .ne. 0) then
    if (debug) write(ERROR_UNIT, *) 'LSD4S:DEINIT_WORKSPACE:'//TRIM(CERROR_MSG(IERR))
    stop 1
  endif
  IF (VERBOSE) THEN
    WRITE(OUTPUT_UNIT, *) '# ALL ARRAYS DEALLOCATED SUCCESFULLY.'
  ENDIF

  !SUBPROGRAMS FOR USE IN THE MAIN BLOCK
  CONTAINS
    SUBROUTINE INIT_WORKSPACE
      IF (.NOT. ALLOCATED(WLCLIN)) THEN
        ALLOCATE(WLCLIN(LINSIZ), STAT=ISTAT)
        IF (ISTAT .NE. 0) THEN
          IERR = CERROR_ALLOC
          if (debug) WRITE(ERROR_UNIT, *) 'LSD4S:INIT_WORKSPACE:'//TRIM(CERROR_MSG(IERR))//':WLCLIN:ISTAT=',ISTAT
        ENDIF
      ENDIF

      IF (.NOT. ALLOCATED(WGTLIN)) THEN
        ALLOCATE(WGTLIN(LSDSIZ,LINSIZ), STAT=ISTAT)
        IF (ISTAT .NE. 0) THEN
          IERR = CERROR_ALLOC
          if (debug) WRITE(ERROR_UNIT, *) 'LSD4S:INIT_WORKSPACE:'//TRIM(CERROR_MSG(IERR))//':WGTLIN:ISTAT=',ISTAT
        ENDIF
      ENDIF
      
      IF (.NOT. ALLOCATED(WL)) THEN
        ALLOCATE(WL(SPSIZ), STAT=ISTAT)
        IF (ISTAT .NE. 0) THEN
          IERR = CERROR_ALLOC
          if (debug) WRITE(ERROR_UNIT, *) 'LSD4S:INIT_WORKSPACE:'//TRIM(CERROR_MSG(IERR))//':WL:ISTAT=',ISTAT
        ENDIF
      ENDIF
      
      IF (.NOT. ALLOCATED(SP)) THEN
        ALLOCATE(SP(SPSIZ), STAT=ISTAT)
        IF (ISTAT .NE. 0) THEN
          IERR = CERROR_ALLOC
          if (debug) WRITE(ERROR_UNIT, *) 'LSD4S:INIT_WORKSPACE:'//TRIM(CERROR_MSG(IERR))//':SP:ISTAT=',ISTAT
        ENDIF
      ENDIF
      
      IF (.NOT. ALLOCATED(SIGSP)) THEN
        ALLOCATE(SIGSP(SPSIZ), STAT=ISTAT)
        IF (ISTAT .NE. 0) THEN
          IERR = CERROR_ALLOC
          if (debug) WRITE(ERROR_UNIT, *) 'LSD4S:INIT_WORKSPACE:'//TRIM(CERROR_MSG(IERR))//':SIGSP:ISTAT=',ISTAT
        ENDIF
      ENDIF
      
      IF (.NOT. ALLOCATED(VLSD)) THEN
        ALLOCATE(VLSD(VSIZ), STAT=ISTAT)
        IF (ISTAT .NE. 0) THEN
          IERR = CERROR_ALLOC
          if (debug) WRITE(ERROR_UNIT, *) 'LSD4S:INIT_WORKSPACE:'//TRIM(CERROR_MSG(IERR))//':VLSD:ISTAT=',ISTAT
        ENDIF
      ENDIF
    end subroutine INIT_WORKSPACE

    subroutine DEINIT_WORKSPACE
      IF (ALLOCATED(WLCLIN)) THEN
        DEALLOCATE(WLCLIN, STAT=ISTAT)
        IF (ISTAT .NE. 0) THEN
          IERR = CERROR_DEALLOC
          if (debug) WRITE(ERROR_UNIT, *) 'LSD4S:INIT_WORKSPACE:'//TRIM(CERROR_MSG(IERR))//':WLCLIN:ISTAT=',ISTAT
        ENDIF
      ENDIF

      IF (ALLOCATED(WGTLIN)) THEN
        DEALLOCATE(WGTLIN, STAT=ISTAT)
        IF (ISTAT .NE. 0) THEN
          IERR = CERROR_DEALLOC
          if (debug) WRITE(ERROR_UNIT, *) 'LSD4S:INIT_WORKSPACE:'//TRIM(CERROR_MSG(IERR))//':WGTLIN:ISTAT=',ISTAT
        ENDIF
      ENDIF
      
      IF (ALLOCATED(WL)) THEN
        DEALLOCATE(WL, STAT=ISTAT)
        IF (ISTAT .NE. 0) THEN
          IERR = CERROR_DEALLOC
          if (debug) WRITE(ERROR_UNIT, *) 'LSD4S:INIT_WORKSPACE:'//TRIM(CERROR_MSG(IERR))//':WL:ISTAT=',ISTAT
        ENDIF
      ENDIF
      
      IF (ALLOCATED(SP)) THEN
        DEALLOCATE(SP, STAT=ISTAT)
        IF (ISTAT .NE. 0) THEN
          IERR = CERROR_DEALLOC
          if (debug) WRITE(ERROR_UNIT, *) 'LSD4S:INIT_WORKSPACE:'//TRIM(CERROR_MSG(IERR))//':SP:ISTAT=',ISTAT
        ENDIF
      ENDIF
      
      IF (ALLOCATED(SIGSP)) THEN
        DEALLOCATE(SIGSP, STAT=ISTAT)
        IF (ISTAT .NE. 0) THEN
          IERR = CERROR_DEALLOC
          if (debug) WRITE(ERROR_UNIT, *) 'LSD4S:INIT_WORKSPACE:'//TRIM(CERROR_MSG(IERR))//':SIGSP:ISTAT=',ISTAT
        ENDIF
      ENDIF
      
      IF (ALLOCATED(PLSD)) THEN
        DEALLOCATE(PLSD, STAT=ISTAT)
        IF (ISTAT .NE. 0) THEN
          IERR = CERROR_dEALLOC
          if (debug) WRITE(ERROR_UNIT, *) 'LSD4S:INIT_WORKSPACE:'//TRIM(CERROR_MSG(IERR))//':PLSD:ISTAT=',ISTAT
        ENDIF
      ENDIF
      
      IF (ALLOCATED(MSP)) THEN
        DEALLOCATE(MSP, STAT=ISTAT)
        IF (ISTAT .NE. 0) THEN
          IERR = CERROR_DEALLOC
          if (debug) WRITE(ERROR_UNIT, *) 'LSD4S:INIT_WORKSPACE:'//TRIM(CERROR_MSG(IERR))//':MSP:ISTAT=',ISTAT
        ENDIF
      ENDIF
      
      IF (ALLOCATED(SIGLSD)) THEN
        DEALLOCATE(SIGLSD, STAT=ISTAT)
        IF (ISTAT .NE. 0) THEN
          IERR = CERROR_DEALLOC
          if (debug) WRITE(ERROR_UNIT, *) 'LSD4S:INIT_WORKSPACE:'//TRIM(CERROR_MSG(IERR))//':SIGLSD:ISTAT=',ISTAT
        ENDIF
      ENDIF
      
      IF (ALLOCATED(VLSD)) THEN
        DEALLOCATE(VLSD, STAT=ISTAT)
        IF (ISTAT .NE. 0) THEN
          IERR = CERROR_DEALLOC
          if (debug) WRITE(ERROR_UNIT, *) 'LSD4S:INIT_WORKSPACE:'//TRIM(CERROR_MSG(IERR))//':VLSD:ISTAT=',ISTAT
        ENDIF
      ENDIF

      IF (ALLOCATED(MSIG)) THEN
        DEALLOCATE(MSIG, STAT=ISTAT)
        IF (ISTAT .NE. 0) THEN
          IERR = CERROR_DEALLOC
          if (debug) WRITE(ERROR_UNIT, *) 'LSD4S:INIT_WORKSPACE:'//TRIM(CERROR_MSG(IERR))//':MSIG:ISTAT=',ISTAT
        ENDIF
      ENDIF
    end subroutine deinit_workspace
END PROGRAM LSD4S
