!------------------------------------------------------------------------------
! Uppsala University, Department of Physics and Astronomy, 
! Least-Squares Deconvolution Code
!------------------------------------------------------------------------------
!
!> @author NRusomarov
!> @date May, 2013
!
!> @brief High-level wrapper for the LSD core subroutines.
!
!> @details The only subroutine here is a high-level wrapper of the LSD core
!> subroutines, so that we don't have to call them all the time when we want
!> to calculate something. Instead, we call run_lsd only once. Since this subroutine
!> is written in modern Fortran we have less clutter in the code. More importantly,
!> the subroutine does not share data with other modules, so it's thread safe.
!------------------------------------------------------------------------------
module lsd_simple
  contains
    !---------------------------------------------------------------------------  
    !> @author NRusomarov
    !> @date April, 2013
    !
    !> @brief High-level wrapper for the lsd_core subroutines.
    !
    !> @note All output arrays are allocated in the subroutine.
    !---------------------------------------------------------------------------  
    subroutine run_lsd(WL, SP, SIGSP,&
                       &WLCLIN, WGTLIN, VLSD,&
                       &REG, LSCL, PREINIT,&
                       &PLSD, SIGLSD, IERR,&
                       &MSIG, MSP, MWGT,&
                       &CHI, MDE, MAXDEV,&
                       &NLINUSE, NWL, debug)
      use cdata, only: i4b, dp
      use cerror
      use lsd_core
      implicit none

      real(kind=dp), intent(inout) :: WL(:)   !< wavelength array
      real(kind=dp), intent(inout) :: SP(:)   !< spectrum array
      real(kind=dp), intent(inout) :: SIGSP(:) !< weights for the spectrum

      real(kind=dp), intent(in) :: WLCLIN(:)   !< central wavelengt for sp. lines
      real(kind=dp), intent(in) :: WGTLIN(:,:) !< weights for the sp. lines
      real(kind=dp), intent(in) :: VLSD(:)  !< velocity space

      real(kind=dp), intent(in) :: REG !< regularization parameter
      LOGICAL, intent(in) :: LSCL !< rescale the LSD errors so that chisq equals to one
      LOGICAL, intent(in) :: PREINIT !< run lsd_preinit if set to .True.

      REAL(kind=dp), allocatable, intent(out) :: PLSD(:) !< LSD profiles array
      REAL(kind=dp), allocatable, intent(out) :: SIGLSD(:) !< errors for the LSD profiles
      INTEGER(kind=i4b), intent(out) :: IERR   !< error code for the subroutine
      logical, intent(in), optional :: debug !< debug flag

      !optional parameters
      real(kind=dp), allocatable, optional, intent(out) :: MSIG(:) !< line weights
      REAL(kind=dp), allocatable, optional, intent(out) :: MSP(:) !< model spectrum
      real(kind=dp), allocatable, optional, intent(out) :: MWGT(:) !< mean weights for the lsd profiles

      REAL(kind=dp), optional, intent(out) :: CHI !< reduced chisq
      REAL(kind=dp), optional, intent(out) :: MDE !< mean std.
      REAL(kind=dp), optional, intent(out) :: MAXDEV !< abs. max. dev.

      integer(kind=i4b), optional, intent(out) :: NLINUSE !< number of lines used
      integer(kind=i4b), optional, intent(out) :: NWL !< number of good spectral pixels in observations array

      !temporaries for the optional parameters
      real(kind=dp), allocatable :: TEMP_MSIG(:) !line weights
      REAL(kind=dp), allocatable :: TEMP_MSP(:)  !model spectrum
      real(kind=dp), allocatable :: TEMP_MWGT(:) !mean weights for the lsd profiles

      REAL(kind=dp)  :: TEMP_CHI !reduced chisq
      REAL(kind=dp)  :: TEMP_MDE !mean std.
      REAL(kind=dp)  :: TEMP_MAXDEV !abs. max. dev.

      integer(kind=i4b) :: TEMP_NLINUSE
      integer(kind=i4b) :: TEMP_NWL

      logical :: TEMP_DEBUG

      !local vars
      real(kind=dp) :: VFIRST
      real(kind=dp) :: VLAST
      real(kind=dp) :: VSTEP
      REAL(kind=dp) :: DA2
      REAL(kind=dp) :: DA1
      INTEGER(kind=i4b) :: LINSIZ !spectral lines
      INTEGER(kind=i4b) :: LSDSIZ !LSD profiles
      INTEGER(kind=i4b) :: SPSIZ  !points in spectrum
      INTEGER(kind=i4b) :: VSIZ   !points in the LSD profile
      LOGICAL :: VERIF = .TRUE.

      INTEGER(kind=i4b) :: ISTAT

      !local allocatables
      real(kind=dp), allocatable :: MM(:,:) !line-pattern matrix

      ierr = 0

      !setting the debug flag
      temp_debug = .false.
      if (present(debug)) temp_debug = debug

      !deal with the sizes of the arrays
      LSDSIZ = size(WGTLIN, 1)
      LINSIZ = size(WGTLIN, 2)
      SPSIZ = size(SP)
      VSIZ = size(VLSD)

      !velocity space params
      vfirst = vlsd(1)
      vlast = vlsd(vsiz)
      vstep = vlsd(2) - vlsd(1) !equidistant velocity grid

      !allocate the output arrays
      !failure to allocate the output arrays leads to fatal error
      allocate(PLSD(VSIZ * LSDSIZ), stat=istat)
      if (istat .ne. 0) then
        ierr = cerror_alloc
        if (temp_debug) write(ERROR_UNIT,*) 'run_lsd:'//trim(cerror_msg(ierr))//':PLSD:istat=',istat
      endif

      allocate(SIGLSD(VSIZ * LSDSIZ), stat=istat)
      if (istat .ne. 0) then
        ierr = cerror_alloc
        if (temp_debug) write(ERROR_UNIT,*) 'run_lsd:'//trim(cerror_msg(ierr))//':SIGLSD:istat=',istat
      endif

      if (present(MSIG)) then
        allocate(MSIG(LINSIZ), stat=istat)
        if (istat .ne. 0) then
          ierr = cerror_alloc
          if (temp_debug) write(ERROR_UNIT,*) 'run_lsd:'//trim(cerror_msg(ierr))//':MSIG:istat=',istat
        endif
      endif


      if (present(MSP)) then
        allocate(MSP(SPSIZ), stat=istat)
        if (istat .ne. 0) then
          ierr = cerror_alloc
          if (temp_debug) write(ERROR_UNIT,*) 'run_lsd:'//trim(cerror_msg(ierr))//':MSP:istat=',istat
        endif
      endif

      if (present(MWGT)) then
        allocate(MWGT(LSDSIZ), stat=istat)
        if (istat .ne. 0) then
          ierr = cerror_alloc
          if (temp_debug) write(ERROR_UNIT,*) 'run_lsd:'//trim(cerror_msg(ierr))//':MWGT:istat=',istat
        endif
      endif

      !allocate the temporary arrays
      allocate(TEMP_MSIG(LINSIZ), STAT=ISTAT)
      if (istat .ne. 0) then
        ierr = cerror_alloc
        if (temp_debug) write(ERROR_UNIT,*) 'run_lsd:'//trim(cerror_msg(ierr))//':TEMP_MSIG:istat=',istat
      endif

      allocate(TEMP_MSP(SPSIZ), STAT=ISTAT)
      if (istat .ne. 0) then
        ierr = cerror_alloc
        if (temp_debug) write(ERROR_UNIT,*) 'run_lsd:'//trim(cerror_msg(ierr))//':TEMP_MSP:istat=',istat
      endif

      allocate(TEMP_MWGT(LSDSIZ), STAT=ISTAT)
      if (istat .ne. 0) then
        ierr = cerror_alloc
        if (temp_debug) write(ERROR_UNIT,*) 'run_lsd:'//trim(cerror_msg(ierr))//':TEMP_MWGT:istat=',istat
      endif

      !if some of the arrays aren't properly allocated there's no point in proceeding further
      if (ierr .ne. 0) return

      !init intent(out) vars
      plsd = 0_dp
      siglsd = 0_dp
      if (present(MSIG)) MSIG=0_dp
      if (present(MSP)) MSP=0_dp
      if (present(MWGT)) MWGT=0_dp
      if (present(CHI)) CHI=0_dp
      if (present(MDE)) MDE=0_dp
      if (present(MAXDEV)) MAXDEV=0_dp
      if (present(NLINUSE)) NLINUSE=0_i4b
      if (present(NWL)) NWL=0_i4b


      !start of computational block

      !REMOVE SPECTRAL PIXELS NOT COVERED BY ANY LINE
      !THE REST OF THE PIXELS ARE MOVED TO THE FRONT OF THE ARRAY
      !NWL GIVES THEIR NUMBER.
      if (preinit) then
        CALL PREINIT_LSD(LINSIZ, SPSIZ,&
                        &WLCLIN, VFIRST, VLAST,&
                        &WL, SP, SIGSP, TEMP_NWL, IERR, temp_debug)
        if (IERR .ne. 0) return
      else
        TEMP_NWL = SPSIZ
      endif

      !ALLOCATE THE LINE-PATTERN MATRIX
      ALLOCATE(MM(TEMP_NWL, LSDSIZ*VSIZ), STAT=ISTAT)
      IF (ISTAT .NE. 0) THEN
        IERR = cerror_alloc
        if (temp_debug) write(ERROR_UNIT,*) 'run_lsd:'//trim(cerror_msg(ierr))//':MM:istat=',istat
        return
      ENDIF

      !COMPUTE LINE-PATTERN MATRIX
      CALL INIT_LSD(LINSIZ, LSDSIZ, SPSIZ, VSIZ, TEMP_NWL,&
                    &WL, SIGSP,&
                    &WLCLIN, WGTLIN,&
                    &VFIRST, VLAST, VSTEP, VLSD,&
                    &MM, TEMP_MWGT, TEMP_MSIG, TEMP_NLINUSE, ierr, temp_debug)
      if (ierr .ne. 0) return

      !COMPUTE LSD PROFILE
      CALL INVERT_LSD(LSDSIZ, SPSIZ, VSIZ, TEMP_NWL,&
                     &MM, SP, SIGSP,&
                     &REG, VERIF, LSCL,&
                     &TEMP_MSP, PLSD, SIGLSD,&
                     &TEMP_CHI, TEMP_MDE, TEMP_MAXDEV,&
                     &DA2, DA1, ierr, temp_debug)
      if (ierr .ne. 0) return

      !end of computational block

      !copy data to optinal vars
      if (present(MSIG)) MSIG=TEMP_MSIG
      if (present(MSP)) MSP=TEMP_MSP
      if (present(MWGT)) MWGT=TEMP_MWGT
      if (present(CHI)) CHI=TEMP_CHI
      if (present(MDE)) MDE=TEMP_MDE
      if (present(MAXDEV)) MAXDEV=TEMP_MAXDEV
      if (present(NLINUSE)) NLINUSE=TEMP_NLINUSE
      if (present(NWL)) NWL=TEMP_NWL


      !deallocate the temporary arrays
      deallocate(TEMP_MSIG, STAT=ISTAT)
      if (istat .ne. 0) then
        ierr = cerror_dealloc
        if (temp_debug) write(ERROR_UNIT,*) 'run_lsd:'//trim(cerror_msg(ierr))//':TEMP_MSIG:istat=',istat
      endif

      deallocate(TEMP_MSP, STAT=ISTAT)
      if (istat .ne. 0) then
        ierr = cerror_dealloc
        if (temp_debug) write(ERROR_UNIT,*) 'run_lsd:'//trim(cerror_msg(ierr))//':TEMP_MSP:istat=',istat
      endif

      deallocate(TEMP_MWGT, STAT=ISTAT)
      if (istat .ne. 0) then
        ierr = cerror_dealloc
        if (temp_debug) write(ERROR_UNIT,*) 'run_lsd:'//trim(cerror_msg(ierr))//':TEMP_MWGT:istat=',istat
      endif

      deallocate(MM, STAT=ISTAT)
      if (istat .ne. 0) then
        ierr = cerror_dealloc
        if (temp_debug) write(ERROR_UNIT,*) 'run_lsd:'//trim(cerror_msg(ierr))//':TEMP_MM:istat=',istat
      endif
    end subroutine run_lsd
end module lsd_simple
