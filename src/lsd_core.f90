!------------------------------------------------------------------------------
! Uppsala University, Department of Physics and Astronomy, 
! Least-Squares Deconvolution Code
!------------------------------------------------------------------------------
!
!> @author NPiskunov
!> @author OKochukhov
!> @author NRusomarov
!> @date March, 2013 - Module subroutines ported.
!> @author NRusomarov
!> @date May, 2013 - Added decent documentation.
!
!> @brief The main LSD subroutines that do the calculations.
!
!> @details Here we have the low level LSD related subroutines that do the calculations.
!> Some of the functions here are also used in my pylsd code, and also in lsd_simple
!> module that contains one Fortran 2003 high-level wrapper of the subroutines here.
!------------------------------------------------------------------------------
MODULE LSD_CORE
  USE CDATA, ONLY : I4B, DP, IOMSG_LEN
  USE CERROR
  USE, INTRINSIC :: ISO_FORTRAN_ENV
  IMPLICIT NONE

  REAL(KIND=DP), PARAMETER :: VC = 299792.458_DP !< speed of light in km/s

  CONTAINS

  !---------------------------------------------------------------------------  
  !> @author NRusomarov
  !> @date April, 2013
  !
  !> @brief Computes equidistant velocity grid for the LSD profiles.
  !---------------------------------------------------------------------------  
  SUBROUTINE SET_VSPACE_LSD(VFIRST, VLAST, VSTEP, VSIZ, VLSD)
    REAL(KIND=DP), INTENT(IN) :: VFIRST   !< first velocity point
    REAL(KIND=DP), INTENT(IN) :: VSTEP    !< velocity step
    INTEGER(KIND=I4B), INTENT(IN) :: VSIZ  !< size of velocity grid
    REAL(KIND=DP), INTENT(INOUT) :: VLAST !< last velocity point
    REAL(KIND=DP), INTENT(OUT) :: VLSD(VSIZ) !< velocity grid array

    INTEGER(KIND=I4B) :: I

    FORALL (I=1:VSIZ) VLSD(I)=VFIRST+VSTEP*(I-1)

    VLAST=VLSD(VSIZ)
  END SUBROUTINE SET_VSPACE_LSD

  !---------------------------------------------------------------------------  
  !> @author NRusomarov
  !> @date April, 2013
  !
  !> @brief Calculates the line-pattern matrix MM.
  !
  !> @remark The line-pattern matrix is constructed as explained in Kochukhov et al. (2011).
  !
  !> @note An important parameter is NWL. It should be returned by preinit_lsd, 
  !> or it should be the same as SPSIZ.
  !---------------------------------------------------------------------------  
  SUBROUTINE INIT_LSD(LINSIZ, LSDSIZ, SPSIZ, VSIZ, NWL,&
                     &WL, SIGSP,&
                     &WLCLIN, WGTLIN,&
                     &VFIRST, VLAST, VSTEP, VLSD,&
                     &MM, MWGT, MSIG, NLINUSE, ierr, debug)
    INTEGER(KIND=I4B), INTENT(IN) :: LINSIZ  !< number of sp. lines
    INTEGER(KIND=I4B), INTENT(IN) :: LSDSIZ  !< number of LSD profiles
    INTEGER(KIND=I4B), INTENT(IN) :: SPSIZ   !< number of pixels for observatons
    INTEGER(KIND=I4B), INTENT(IN) :: VSIZ    !< size of velocity grid
    INTEGER(KIND=I4B), INTENT(IN) :: NWL     !< number of usable pixels from observations.

    REAL(KIND=DP), INTENT(IN) :: WL(SPSIZ)    !< wavelengths array for observations
    REAL(KIND=DP), INTENT(IN) :: SIGSP(SPSIZ) !< weights array for observations

    REAL(KIND=DP), INTENT(IN) :: WLCLIN(LINSIZ)         !< central wavelengths for sp. lines
    REAL(KIND=DP), INTENT(IN) :: WGTLIN(LSDSIZ, LINSIZ) !< weights matrix for sp. lines

    REAL(KIND=DP), INTENT(IN) :: VFIRST !< first velocity point
    REAL(KIND=DP), INTENT(IN) :: VLAST !< last velocity point
    REAL(KIND=DP), INTENT(IN) :: VSTEP !< velocity step
    REAL(KIND=DP), INTENT(IN) :: VLSD(VSIZ) !< array for velocity space

    REAL(KIND=DP), INTENT(OUT) :: MM(NWL,LSDSIZ*VSIZ) !< line-pattern matrix
    REAL(KIND=DP), INTENT(OUT) :: MWGT(LSDSIZ) !< mean weight for the LSD profiles
    REAL(KIND=DP), INTENT(OUT) :: MSIG(LINSIZ) !< line weights after computation of the line-pattern matrix

    INTEGER(KIND=I4B), INTENT(OUT) :: NLINUSE !< number of spectral lines used in the computation of the line-pattern matrix
    INTEGER(KIND=I4B), INTENT(OUT) :: IERR     !< error code
    logical, intent(in), optional :: debug !< debug flag

    !TMP VARS
    INTEGER(KIND=I4B), ALLOCATABLE :: LUSE(:)
    INTEGER(KIND=I4B) :: ISTAT

    REAL(KIND=DP), ALLOCATABLE     :: SSIG(:)
    REAL(KIND=DP), ALLOCATABLE     :: DV(:)
    REAL(KIND=DP)     :: NWGT

    INTEGER(KIND=I4B) :: IWL, ILSD, ILINE, II, IV
    logical :: temp_debug

    !set the debug flag
    temp_debug = .false.
    if (present(debug)) temp_debug = debug

    !INIT INTENT(OUT) VARS
    MM = 0_dp
    MWGT = 0_dp
    MSIG = 0_dp
    NLINUSE = 0_i4b
    ierr = 0_i4b

    ! INITIALIZE COUNTER FOR LINE CONTRIBUTION AND ACCUMULATOR FOR MEAN LINE 1/SIGMA^2
    ALLOCATE(LUSE(LINSIZ), STAT=ISTAT)
    IF (ISTAT .NE. 0) THEN
      ierr = cerror_alloc
      if (temp_debug) WRITE(ERROR_UNIT, *) 'INIT_LSD:'//TRIM(CERROR_MSG(ierr))//':LUSE:ISTAT=',ISTAT
      return
    ENDIF

    ALLOCATE(SSIG(SPSIZ), STAT=ISTAT)
    IF (ISTAT .NE. 0) THEN
      if (temp_debug) WRITE(ERROR_UNIT, *) 'INIT_LSD:'//TRIM(CERROR_MSG(ierr))//':SSIG:ISTAT=',ISTAT
      return
    ENDIF

    ALLOCATE(DV(LINSIZ), STAT=ISTAT)
    IF (ISTAT .NE. 0) THEN
      if (temp_debug) WRITE(ERROR_UNIT, *) 'INIT_LSD:'//TRIM(CERROR_MSG(ierr))//':DV:ISTAT=',ISTAT
      return
    ENDIF

    LUSE = 0_i4b
    MSIG = 0_DP
    SSIG=SIGSP**2
    MM = 0_DP

    !  LOOP THROUGH WAVELENGTH POINTS
    DO IWL=1,NWL

      DV=(WL(IWL)-WLCLIN)/WLCLIN*VC
      !  LOOP THROUGH LINE LIST
      DO ILINE=1,LINSIZ

          !  CONSIDER THE LINE IF IT FALLS WITHIN LSD VELOCITY GRID FROM CURRENT POINT
          IF(DV(ILINE).GT.VFIRST.AND.DV(ILINE).LT.VLAST) THEN
            LUSE(ILINE)=LUSE(ILINE)+1
            IV=INT((DV(ILINE)-VFIRST)/VSTEP)+1

            !  ACCUMULATE 1/SIGMA^2 FOR EACH LINE
            MSIG(ILINE)=MSIG(ILINE)+SSIG(IWL)

            !  LOOP OVER LSD PROFILES
            DO ILSD=1,LSDSIZ
              II=IV+(ILSD-1)*VSIZ

              !  ADD LINEAR CONTRIBUTION TO THE LINE-PATTERN MATRIX
              MM(IWL,II+1) = MM(IWL,II+1) + &
                           & WGTLIN(ILSD,ILINE)*(DV(ILINE)-VLSD(IV  ))/VSTEP
              MM(IWL,II  ) = MM(IWL,II  ) + &
                           & WGTLIN(ILSD,ILINE)*(VLSD(IV+1)-DV(ILINE))/VSTEP
            ENDDO
          END IF
      ENDDO
    ENDDO

    !  INITIALIZE COUNTERS AND ACCUMULATORS
    NLINUSE=0
    NWGT=0_DP
    MWGT=0_DP

    !  COUNT LINES USED TO CONSTRUCT LINE-PATTERN MATRIX AND COMPUTE WEIGHTED MEAN LINE WEIGHT
    DO ILINE=1,LINSIZ
      IF(LUSE(ILINE).GT.0) THEN
        NLINUSE=NLINUSE+1
        NWGT=NWGT+MSIG(ILINE)/LUSE(ILINE)
        DO ILSD=1,LSDSIZ
          MWGT(ILSD) = MWGT(ILSD) + WGTLIN(ILSD,ILINE) &
                     & * MSIG(ILINE)/LUSE(ILINE)
        ENDDO
      END IF
    ENDDO
    MWGT = MWGT / NWGT

    ! INITIALIZE COUNTER FOR LINE CONTRIBUTION AND ACCUMULATOR FOR MEAN LINE 1/SIGMA^2
    DEALLOCATE(LUSE, STAT=ISTAT)
    IF (ISTAT .NE. 0) THEN
      ierr = cerror_dealloc
      if (temp_debug) WRITE(ERROR_UNIT, *) 'INIT_LSD:'//TRIM(CERROR_MSG(ierr))//':LUSE:ISTAT=',ISTAT
    ENDIF

    DEALLOCATE(SSIG, STAT=ISTAT)
    IF (ISTAT .NE. 0) THEN
      ierr = cerror_dealloc
      if (temp_debug) WRITE(ERROR_UNIT, *) 'INIT_LSD:'//TRIM(CERROR_MSG(ierr))//':SSIG:ISTAT=',ISTAT
    ENDIF

    DEALLOCATE(DV, STAT=ISTAT)
    IF (ISTAT .NE. 0) THEN
      ierr = cerror_dealloc
      if (temp_debug) WRITE(ERROR_UNIT, *) 'INIT_LSD:'//TRIM(CERROR_MSG(ierr))//':DV:ISTAT=',ISTAT
    ENDIF
  END SUBROUTINE INIT_LSD

  !---------------------------------------------------------------------------  
  !> @author NRusomarov
  !> @date April, 2013
  !
  !> @brief Calculates the inverse of the autocorrelation matrix.
  !
  !> @warning This is where we make calls to math.f or external math subroutines.
  !---------------------------------------------------------------------------  
  SUBROUTINE LINEQ(AM,AI,N,INFO)
    INTEGER(KIND=I4B), INTENT(IN) :: N
    REAL(KIND=DP), INTENT(IN) :: AM(N,N)
    REAL(KIND=DP), INTENT(OUT) :: AI(N,N)
    INTEGER(KIND=I4B), INTENT(OUT) :: INFO

    !TMP VARS
    INTEGER(KIND=I4B) :: I, J
    CHARACTER(LEN=1), PARAMETER :: INP_STR = 'U'

    !COPY TO NEW MATRIX
    AI = AM

    !TWO CALLS TO EXTERNAL LIBRARIES
    CALL DPOTRF(INP_STR,N,AI,N,INFO)
    CALL DPOTRI(INP_STR,N,AI,N,INFO)

    DO I=2,N
      DO J=1,I-1
        AI(I,J)=AI(J,I)
      ENDDO
    ENDDO

  END SUBROUTINE LINEQ

  !---------------------------------------------------------------------------  
  !> @author NRusomarov
  !> @date April, 2013
  !
  !> @brief Preinits the observation arrays and gives us NWL.
  !
  !> @details This subroutine goes through each wavelength point and checks if
  !> it contributes to any spectral line in the range (VFIRST, VLAST). Once we have
  !> marked the useful observation pixels we shift them to left. The parameter
  !> NWL gives us the number of useful observation pixels.
  !---------------------------------------------------------------------------  
  SUBROUTINE PREINIT_LSD(LINSIZ, SPSIZ,&
                        &WLCLIN, VFIRST, VLAST,&
                        &WL, SP, SIGSP, NWL, ierr, debug)
    INTEGER(KIND=I4B), INTENT(IN) :: LINSIZ !< number of spectral lines
    INTEGER(KIND=I4B), INTENT(IN) :: SPSIZ  !< number of observation pixels

    REAL(KIND=DP), INTENT(IN) :: WLCLIN(LINSIZ) !< central wavelegth of sp. lines
    REAL(KIND=DP), INTENT(IN) :: VFIRST !< first velocity point
    REAL(KIND=DP), INTENT(IN) :: VLAST !< last velocity point

    REAL(KIND=DP), INTENT(INOUT) :: WL(SPSIZ) !< wavelengths array
    REAL(KIND=DP), INTENT(INOUT) :: SP(SPSIZ) !< array with observations spectrum
    REAL(KIND=DP), INTENT(INOUT) :: SIGSP(SPSIZ) !< array with weights for the observations spectrum

    INTEGER(KIND=I4B), INTENT(OUT) :: NWL !< number of useful observation pixels.
    INTEGER(KIND=I4B), INTENT(OUT) :: IERR     !< error code
    logical, intent(in), optional :: debug !< debug flag

    !TMP VARS
    REAL(KIND=DP) :: DV
    INTEGER(KIND=I4B) :: JWL, IWL, ILINE

    INTEGER(KIND=I4B), ALLOCATABLE :: IUSE(:)
    INTEGER(KIND=I4B) :: ISTAT
    logical :: temp_debug
    
    !set the debug flag
    temp_debug = .false.
    if (present(debug)) temp_debug = debug

    !INIT INTENT(OUT) VARS
    NWL = 0_i4b
    IERR = 0_i4b

    ALLOCATE(IUSE(SPSIZ), STAT=ISTAT)
    IF (ISTAT .NE. 0) THEN
      ierr = cerror_alloc
      if (temp_debug) WRITE(ERROR_UNIT, *) 'PREINIT_LSD:'//TRIM(CERROR_MSG(IERR))//':IUSE:ISTAT=',ISTAT
      return
    ENDIF

    ! LOOP OVER WAVELENGTH POINTS
    JWL=1
    DO IWL=1,SPSIZ

      ! LOOP THROUGH THE LINE LIST
      DO ILINE=1,LINSIZ

        ! CONSIDER THE LINE IF IT FALLS WITHIN LSD VELOCITY RANGE FROM THE CURRENT POINT
        DV=(WL(IWL)-WLCLIN(ILINE))/WLCLIN(ILINE)*VC
        IF(DV.GT.VFIRST.AND.DV.LT.VLAST) THEN
          IUSE(JWL)=IWL
          JWL=JWL+1
          EXIT
        END IF
      ENDDO
    ENDDO

    ! REMOVE UNUSED WAVELENGTH POINTS BY SHIFTING USED ONES TO THE LEFT
    NWL=JWL-1
    DO IWL=1,NWL
      JWL=IUSE(IWL)
      WL(IWL)=WL(JWL)
      SP(IWL)=SP(JWL)
      SIGSP(IWL)=SIGSP(JWL)
    ENDDO

    !DEALLOC TMP ARRAY
    DEALLOCATE(IUSE, STAT=ISTAT)
    IF (ISTAT .NE. 0) THEN
      ierr = cerror_dealloc
      if (temp_debug) WRITE(ERROR_UNIT, *) 'PREINIT_LSD:'//TRIM(CERROR_MSG(IERR))//':IUSE:ISTAT=',ISTAT
    ENDIF
  END SUBROUTINE PREINIT_LSD

  !---------------------------------------------------------------------------  
  !> @author NRusomarov
  !> April, 2013 - First written.
  !
  !> @brief Computes the LSD profiles and synthetic spectrum.
  !
  !> @details to be written
  !---------------------------------------------------------------------------  
  SUBROUTINE INVERT_LSD(LSDSIZ, SPSIZ, VSIZ, NWL,&
                       &MM, SP, SIGSP,&
                       &REG, VERIF, LSCL,&
                       &MSP, PLSD, SIGLSD,&
                       &CHI, MDE, MAXDEV,&
                       &DA2, DA1, IERR, debug)
    INTEGER(KIND=I4B), INTENT(IN) :: LSDSIZ !< number of sp. lines
    INTEGER(KIND=I4B), INTENT(IN) :: SPSIZ  !< size of observation arrays
    INTEGER(KIND=I4B), INTENT(IN) :: VSIZ   !< size of velocity grid
    INTEGER(KIND=I4B), INTENT(IN) :: NWL    !< number of useful observation pixels

    REAL(KIND=DP), INTENT(IN) :: MM(NWL,LSDSIZ*VSIZ) !< line-pattern matrix
    REAL(KIND=DP), INTENT(IN) :: SP(SPSIZ)           !< observations spectrum
    REAL(KIND=DP), INTENT(IN) :: SIGSP(SPSIZ)        !< weights for observations spectrum

    REAL(KIND=DP), INTENT(IN) :: REG                 !< regularization parameter

    LOGICAL, INTENT(IN) :: VERIF                     !< verify errors when inverting matrix
    LOGICAL, INTENT(IN) :: LSCL                      !< rescale errors for LSD profiles so that chisq equals to one

    REAL(KIND=DP), INTENT(OUT) :: MSP(SPSIZ)         !< MODEL SPECTRUM
    REAL(KIND=DP), INTENT(OUT) :: PLSD(LSDSIZ*VSIZ)  !< LSD PROFILES
    REAL(KIND=DP), INTENT(OUT) :: SIGLSD(LSDSIZ*VSIZ) !< errors FOR LSD PROFILES

    REAL(KIND=DP), INTENT(OUT) :: CHI !< REDUCED CHI^2 of the resulting LSD profile
    REAL(KIND=DP), INTENT(OUT) :: MDE !< MEAN STANDARD DEVIATION of the observations (?)
    REAL(KIND=DP), INTENT(OUT) :: MAXDEV !< MAX. STD. DEV. of (MSP - SP)

    REAL(KIND=DP), INTENT(OUT) :: DA1 !< MAX ERRORS
    REAL(KIND=DP), INTENT(OUT) :: DA2 !< MAX ERRORS along diagonal of AM*AI

    INTEGER(KIND=I4B), INTENT(OUT) :: IERR !< ERROR CODES
    logical, intent(in), optional :: debug !< debug flag


    !LOCAL VARS
    INTEGER(KIND=I4B) :: N
    INTEGER(KIND=I4B) :: I1, I2
    REAL(KIND=DP) :: REG1
    INTEGER(KIND=I4B) :: ISTAT
    REAL(KIND=DP) :: VAL
    REAL(KIND=DP) :: tr1_tmp
    REAL(KIND=DP) :: tr2_tmp
    logical :: temp_debug

    !ALLOCATABLES
    REAL(KIND=DP), ALLOCATABLE :: AM(:,:)
    REAL(KIND=DP), ALLOCATABLE :: AI(:,:)
    REAL(KIND=DP), ALLOCATABLE :: TR1(:)
    REAL(KIND=DP), ALLOCATABLE :: TR2(:)
    REAL(KIND=DP), ALLOCATABLE :: DEV_RES(:)

    !ITERATORS
    INTEGER(KIND=I4B) :: I, J
    INTEGER(KIND=I4B) :: IWL
    INTEGER(KIND=I4B) :: ILSD

    !set the debug flag
    temp_debug = .false.
    if (present(debug)) temp_debug = debug

    !INIT INTENT(OUT) VARS
    MSP    = 0_dp
    PLSD   = 0_dp
    SIGLSD = 0_dp
    CHI    = 0_dp
    MDE    = 0_dp
    MAXDEV = 0_dp
    DA1    = 0_dp
    DA2    = 0_dp
    IERR   = 0_i4b


    ! TOTAL NUMBER OF ELEMENTS IN THE LSD PROFILES
    N=LSDSIZ * VSIZ

    !ALLOCATE TMP ARRAYS
    ALLOCATE(AM(N,N), STAT=ISTAT)
    IF (ISTAT .NE. 0) THEN
      ierr = cerror_alloc
      if (temp_debug) WRITE(ERROR_UNIT, *) 'INVERT_LSD:'//TRIM(CERROR_MSG(ierr))//':AM:ISTAT=',ISTAT
      return
    ENDIF

    ALLOCATE(AI(N,N), STAT=ISTAT)
    IF (ISTAT .NE. 0) THEN
      ierr = cerror_alloc
      if (temp_debug) WRITE(ERROR_UNIT, *) 'INVERT_LSD:'//TRIM(CERROR_MSG(ierr))//':AI:ISTAT=',ISTAT
      return
    ENDIF
    
    ALLOCATE(TR1(LSDSIZ), STAT=ISTAT)
    IF (ISTAT .NE. 0) THEN
      ierr = cerror_alloc
      if (temp_debug) WRITE(ERROR_UNIT, *) 'INVERT_LSD:'//TRIM(CERROR_MSG(ierr))//':TR1:ISTAT=',ISTAT
      return
    ENDIF
    
    ALLOCATE(TR2(LSDSIZ), STAT=ISTAT)
    IF (ISTAT .NE. 0) THEN
      ierr = cerror_alloc
      if (temp_debug) WRITE(ERROR_UNIT, *) 'INVERT_LSD:'//TRIM(CERROR_MSG(ierr))//':TR2:ISTAT=',ISTAT
      return
    ENDIF

    ALLOCATE(DEV_RES(NWL), STAT=ISTAT)
    IF (ISTAT .NE. 0) THEN
      ierr = cerror_alloc
      if (temp_debug) WRITE(ERROR_UNIT, *) 'INVERT_LSD:'//TRIM(CERROR_MSG(ierr))//':DEV_RES:ISTAT=',ISTAT
      return
    ENDIF

    ! FILL UPPER TRIANGLE OF THE SQUARE SYMMETRIC 
    ! AUTOCORRELATION MATRIX: AM = M^T # S^2 # M
    AM = 0_DP
    do I = 1, N
      do J = I, N
        AM(I,J)=SUM(MM(:,I)*MM(:,J)*SIGSP(1:NWL)**2)
      enddo
    enddo

    ! COMPUTE NORMALIZED TRACES OF THE MAIN DIAGONAL AND SUB-DIAGONAL
    TR1=0_DP
    TR2=0_DP
    DO ILSD=1,LSDSIZ
      I1=(ILSD-1)*VSIZ+1
      I2=I1+VSIZ-1
      DO J=I1,I2
        TR1(ILSD)=TR1(ILSD)+AM(J,J)/VSIZ
        IF(J.LT.I2) TR2(ILSD)=TR2(ILSD)+AM(J,J+1)/(VSIZ-1)
      ENDDO
    ENDDO

    ! APPLY OPTIONAL TIKHONOV REGULARIZATION BY MODIFYING 3-DIAGONAL
    ! PART OF THE UPPER TRIANGLE OF AUTOCORRELATION MATRIX
    IF(REG.GT.1d-7) THEN
      DO ILSD=1,LSDSIZ
        REG1=REG*TR1(ILSD)
        I1=(ILSD-1)*VSIZ+1
        I2=I1+VSIZ-1

        ! FIRST AND LAST ROWS
        AM(I1,I1  )=AM(I1,I1  )+REG1
        AM(I1,I1+1)=AM(I1,I1+1)-REG1
        AM(I2,I2  )=AM(I2,I2  )+REG1

        ! OTHER ROWS
        DO J=I1+1,I2-1
          AM(J,J  )=AM(J,J  )+2.D0*REG1
          AM(J,J+1)=AM(J,J+1)-     REG1
        ENDDO
      ENDDO
    END IF

    ! FILL LOWER TRIANGLE ASSUMING SYMMETRY
    DO I=2,N
      DO J=1,I-1
        AM(I,J)=AM(J,I)
      ENDDO
    ENDDO

    ! INVERT AUTOCORRELATION MATRIX BY SOLVING SYSTEM OF LINEAR EQUATIONS
    CALL LINEQ(AM,AI,N,IERR)                  ! VERSION 4

    !  VERIFY CORRECTNESS OF AUTOCORRELATION MATRIX INVERSION
    IF(IERR.NE.0) THEN
      if (temp_debug) WRITE(ERROR_UNIT,*) 'INVERT_LSD:LINEQ:'//CERROR_MSG(IERR)//':INFO=',IERR
      IERR = CERROR_INVERT_MATRIX
      return
    ENDIF

    !NR: This looks like remains from older code.
    ! COMPUTE MAXIMUM DEVIATIONS OF AM * AI FROM THE UNITY MATRIX
    IF(VERIF) THEN
      DA1=0_DP
      DA2=0_DP
      DO I=1,N
        DO J=1,N
          VAL=SUM(AM(I,:)*AI(:,J))
          IF(I.EQ.J) THEN
            DA2=MAX(DA2,ABS(VAL-1_DP))
          ELSE
            DA1=MAX(DA1,ABS(VAL))
          ENDIF
        ENDDO
      ENDDO
    ENDIF

    ! MULTIPLY MATRIX M^T # S^2 BY SPECTRUM ARRAY SP
    FORALL (I=1:N) MSP(I) = SUM(MM(:,I) * SP(1:NWL) * SIGSP(1:NWL)**2)

    ! MULTIPLY MATRIX AI BY VECTOR MSP TO OBTAIN LSD PROFILE
    DO I=1,N
        PLSD(I)=SUM(AI(1:N,I)*MSP(1:N))
    ENDDO

    ! APPLY DEFAULT REGULARIZATION DIRECTLY TO LSD PROFILE
    IF(REG.LT.-1d-7) THEN
      DO ILSD=1,LSDSIZ
        I1=(ILSD-1)*VSIZ+1
        I2=I1+VSIZ-1
        tr1_tmp=TR1(ILSD)/(TR1(ILSD)+TR2(ILSD))
        tr2_tmp=TR2(ILSD)/(TR1(ILSD)+TR2(ILSD))
        DO J=I1+1,I2-1
          PLSD(J)=0.5_DP*tr2_tmp*(PLSD(J-1)+PLSD(J+1))+tr1_tmp*PLSD(J)
        ENDDO
      ENDDO
    ENDIF

    ! LSD ERROR BARS
    forall (I=1:N) SIGLSD(I)=SQRT(AI(I,I))

    ! COMPUTE MODEL SPECTRUM MSP (MM # PLSD), REDUCED CHI^2, 
    DO IWL=1,NWL
        MSP(IWL)=SUM(MM(IWL,:)*PLSD(:))
    ENDDO

    ! CALC DEVIATION
    DEV_RES = (MSP(1:NWL)-SP(1:NWL))*SIGSP(1:NWL)

    ! MAXIMUM DEVIATION
    MAXDEV=MAXVAL(ABS(DEV_RES))

    ! MEAN STANDARD DEVIATION
    MDE=SUM((MSP(1:NWL)-SP(1:NWL))**2)
    MDE=SQRT(MDE/NWL)

    ! REDUCED CHI^2
    CHI=sum(DEV_RES**2)
    CHI=CHI/(NWL-N)

    !  SCALE LSD ERROR BARS
    IF(LSCL) THEN
      SIGLSD=SIGLSD*SQRT(CHI)
    ENDIF

    !DEALLOCATE TMP ARRAYS
    DEALLOCATE(AM, STAT=ISTAT)
    IF (ISTAT .NE. 0) THEN
      IERR = cerror_dealloc
      if (temp_debug) WRITE(ERROR_UNIT, *) 'INVERT_LSD:'//TRIM(CERROR_MSG(CERROR_ALLOC))//':AM:ISTAT=',ISTAT
    ENDIF

    DEALLOCATE(AI, STAT=ISTAT)
    IF (ISTAT .NE. 0) THEN
      IERR = cerror_dealloc
      if (temp_debug) WRITE(ERROR_UNIT, *) 'INVERT_LSD:'//TRIM(CERROR_MSG(CERROR_ALLOC))//':AI:ISTAT=',ISTAT
    ENDIF
    
    DEALLOCATE(TR1, STAT=ISTAT)
    IF (ISTAT .NE. 0) THEN
      IERR = cerror_dealloc
      if (temp_debug) WRITE(ERROR_UNIT, *) 'INVERT_LSD:'//TRIM(CERROR_MSG(CERROR_ALLOC))//':TR1:ISTAT=',ISTAT
    ENDIF
    
    DEALLOCATE(TR2, STAT=ISTAT)
    IF (ISTAT .NE. 0) THEN
      IERR = cerror_dealloc
      if (temp_debug) WRITE(ERROR_UNIT, *) 'INVERT_LSD:'//TRIM(CERROR_MSG(CERROR_ALLOC))//':TR2:ISTAT=',ISTAT
    ENDIF

    DEALLOCATE(DEV_RES, STAT=ISTAT)
    IF (ISTAT .NE. 0) THEN
      IERR = cerror_dealloc
      if (temp_debug) WRITE(ERROR_UNIT, *) 'INVERT_LSD:'//TRIM(CERROR_MSG(CERROR_ALLOC))//':DEV_RES:ISTAT=',ISTAT
    ENDIF
  END SUBROUTINE INVERT_LSD
END MODULE LSD_CORE
