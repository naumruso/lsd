!------------------------------------------------------------------------------
! Uppsala University, Department of Physics and Astronomy, 
! Least-Squares Deconvolution Code
!------------------------------------------------------------------------------
!
!> @author NRusomarov
!> @date March, 2013
!
!> @brief All LSD related variables.
!
!> @details This module contains all of the variables that are required in
!> to run the LSD code. It also contains two additional subroutines for
!> initialization and deinitialization of the allocatable arrays.
!------------------------------------------------------------------------------
module lsd4_data
  use cdata, only : i4b, dp
  use cerror
  use, intrinsic :: iso_fortran_env
  implicit none

  public

  !constant program params
  LOGICAL :: VERIF = .TRUE.   !< confirm that errors of the inverse matrix are okay
  LOGICAL :: LSCL = .TRUE.    !< rescale profiles to sqrt(chi)
  LOGICAL :: VERBOSE = .TRUE. !< print additional data to std out
  LOGICAL :: DEBUG = .TRUE.   !< print debug data to std err

  !all other variables
  INTEGER(kind=i4b) :: LINSIZ !< number of spectral lines
  INTEGER(kind=i4b) :: LSDSIZ !< number of LSD profiles
  INTEGER(kind=i4b) :: SPSIZ  !< number of points in spectrum
  INTEGER(kind=i4b) :: VSIZ   !< number of points in the LSD profile
  real(kind=dp), allocatable :: WLCLIN(:)  !< central wavelengths for spectral lines
  real(kind=dp), allocatable :: WGTLIN(:,:)!< input weights for the sp. lines
  real(kind=dp), allocatable :: WL(:)      !< spectrum wavelengths
  real(kind=dp), allocatable :: SP(:)      !< observed spectrum
  real(kind=dp), allocatable :: SIGSP(:)   !< errors/weights for observed spectrum
  REAL(kind=dp), allocatable :: MSP(:)     !< modeled spectrum
  REAL(kind=dp), allocatable :: PLSD(:)    !< resulting LSD profiles
  REAL(kind=dp), allocatable :: SIGLSD(:)  !< sigma errors for the LSD profiles
  real(kind=dp), allocatable :: VLSD(:)    !< velocity space for LSD profiles
  real(kind=dp), allocatable :: MM(:,:)    !< line-pattern matrix
  real(kind=dp), allocatable :: MWGT(:)    !< mean weights for the lsd profiles
  real(kind=dp), allocatable :: MSIG(:)    !< output line weights for the sp. lines
  real(kind=dp) :: VFIRST                  !< first velocity point
  real(kind=dp) :: VLAST                   !< last velocity point
  real(kind=dp) :: VSTEP                   !< velocity step
  real(kind=dp) :: REG                     !< regularization parameter
  REAL(kind=dp) :: CHI                     !< reduced chisq
  REAL(kind=dp) :: MDE                     !< mean std.
  REAL(kind=dp) :: MAXDEV                  !< abs. max. dev. of (MSP - SP)
  REAL(kind=dp) :: DA2
  REAL(kind=dp) :: DA1
  integer(kind=i4b) :: POL                 !< polarization parameter
  integer(kind=i4b) :: NWL                 !< number of used wavelength points for the creation of MM
  integer(kind=i4b) :: NLINUSE             !< number of used sp. lines for the creation of MM

  contains

  !---------------------------------------------------------------------------  
  !> @author NRusomarov
  !> @date April, 2013
  !
  !> @brief Deallocates the working arrays.
  !---------------------------------------------------------------------------  
  SUBROUTINE DEINIT_WORKSPACE_LSD(WLCLIN,WGTLIN,WL,SP,SIGSP,&
                                 &MSP,VLSD,PLSD,SIGLSD,MWGT,MSIG,MM,ierr, debug)
    REAL(KIND=DP), ALLOCATABLE, INTENT(OUT) :: WLCLIN(:)
    REAL(KIND=DP), ALLOCATABLE, INTENT(OUT) :: WGTLIN(:,:)
    REAL(KIND=DP), ALLOCATABLE, INTENT(OUT) :: WL(:)
    REAL(KIND=DP), ALLOCATABLE, INTENT(OUT) :: SP(:)
    REAL(KIND=DP), ALLOCATABLE, INTENT(OUT) :: SIGSP(:)
    REAL(KIND=DP), ALLOCATABLE, INTENT(OUT) :: MSP(:)
    REAL(KIND=DP), ALLOCATABLE, INTENT(OUT) :: PLSD(:)
    REAL(KIND=DP), ALLOCATABLE, INTENT(OUT) :: SIGLSD(:)
    REAL(KIND=DP), ALLOCATABLE, INTENT(OUT) :: VLSD(:)
    REAL(KIND=DP), ALLOCATABLE, INTENT(OUT) :: MWGT(:)
    REAL(KIND=DP), ALLOCATABLE, INTENT(OUT) :: MSIG(:)
    REAL(KIND=DP), ALLOCATABLE, INTENT(OUT) :: MM(:,:)

    INTEGER(KIND=I4B), intent(out) :: ierr
    logical, intent(in), optional :: debug

    !TMP VARS
    INTEGER(KIND=I4B) :: ISTAT
    logical :: temp_debug

    ierr = 0
    
    !set debug flag
    temp_debug = .false.
    if (present(debug)) temp_debug = debug


    IF (ALLOCATED(MM)) THEN
      DEALLOCATE(MM, STAT=ISTAT)
      IF (ISTAT .NE. 0) THEN
        ierr = cerror_dealloc
        if (temp_debug) WRITE(ERROR_UNIT, *) 'DEINIT_WORKSPACE_LSD:'//TRIM(CERROR_MSG(CERROR_DEALLOC))//':MM:ISTAT=',ISTAT
      ENDIF
    ENDIF

    IF (ALLOCATED(WLCLIN)) THEN
      DEALLOCATE(WLCLIN, STAT=ISTAT)
      IF (ISTAT .NE. 0) THEN
        ierr = cerror_dealloc
        if (temp_debug) WRITE(ERROR_UNIT, *) 'DEINIT_WORKSPACE_LSD:'//TRIM(CERROR_MSG(CERROR_DEALLOC))//':WLCLIN:ISTAT=',ISTAT
      ENDIF
    ENDIF

    IF (ALLOCATED(WGTLIN)) THEN
      DEALLOCATE(WGTLIN, STAT=ISTAT)
      IF (ISTAT .NE. 0) THEN
        ierr = cerror_dealloc
        if (temp_debug) WRITE(ERROR_UNIT, *) 'DEINIT_WORKSPACE_LSD:'//TRIM(CERROR_MSG(CERROR_DEALLOC))//':WGTLIN:ISTAT=',ISTAT
      ENDIF
    ENDIF
    
    IF (ALLOCATED(WL)) THEN
      DEALLOCATE(WL, STAT=ISTAT)
      IF (ISTAT .NE. 0) THEN
        ierr = cerror_dealloc
        if (temp_debug) WRITE(ERROR_UNIT, *) 'DEINIT_WORKSPACE_LSD:'//TRIM(CERROR_MSG(CERROR_DEALLOC))//':WL:ISTAT=',ISTAT
      ENDIF
    ENDIF
    
    IF (ALLOCATED(SP)) THEN
      DEALLOCATE(SP, STAT=ISTAT)
      IF (ISTAT .NE. 0) THEN
        ierr = cerror_dealloc
        if (temp_debug) WRITE(ERROR_UNIT, *) 'DEINIT_WORKSPACE_LSD:'//TRIM(CERROR_MSG(CERROR_DEALLOC))//':SP:ISTAT=',ISTAT
      ENDIF
    ENDIF
    
    IF (ALLOCATED(SIGSP)) THEN
      DEALLOCATE(SIGSP, STAT=ISTAT)
      IF (ISTAT .NE. 0) THEN
        ierr = cerror_dealloc
        if (temp_debug) WRITE(ERROR_UNIT, *) 'DEINIT_WORKSPACE_LSD:'//TRIM(CERROR_MSG(CERROR_DEALLOC))//':SIGSP:ISTAT=',ISTAT
      ENDIF
    ENDIF
    
    IF (ALLOCATED(PLSD)) THEN
      DEALLOCATE(PLSD, STAT=ISTAT)
      IF (ISTAT .NE. 0) THEN
        ierr = cerror_dealloc
        if (temp_debug) WRITE(ERROR_UNIT, *) 'DEINIT_WORKSPACE_LSD:'//TRIM(CERROR_MSG(CERROR_DEALLOC))//':PLSD:ISTAT=',ISTAT
      ENDIF
    ENDIF
    
    IF (ALLOCATED(MSP)) THEN
      DEALLOCATE(MSP, STAT=ISTAT)
      IF (ISTAT .NE. 0) THEN
        ierr = cerror_dealloc
        if (temp_debug) WRITE(ERROR_UNIT, *) 'DEINIT_WORKSPACE_LSD:'//TRIM(CERROR_MSG(CERROR_DEALLOC))//':MSP:ISTAT=',ISTAT
      ENDIF
    ENDIF
    
    IF (ALLOCATED(SIGLSD)) THEN
      DEALLOCATE(SIGLSD, STAT=ISTAT)
      IF (ISTAT .NE. 0) THEN
        ierr = cerror_dealloc
        if (temp_debug) WRITE(ERROR_UNIT, *) 'DEINIT_WORKSPACE_LSD:'//TRIM(CERROR_MSG(CERROR_DEALLOC))//':SIGLSD:ISTAT=',ISTAT
      ENDIF
    ENDIF
    
    IF (ALLOCATED(VLSD)) THEN
      DEALLOCATE(VLSD, STAT=ISTAT)
      IF (ISTAT .NE. 0) THEN
        ierr = cerror_dealloc
        if (temp_debug) WRITE(ERROR_UNIT, *) 'DEINIT_WORKSPACE_LSD:'//TRIM(CERROR_MSG(CERROR_DEALLOC))//':VLSD:ISTAT=',ISTAT
      ENDIF
    ENDIF

    IF (ALLOCATED(MSIG)) THEN
      DEALLOCATE(MSIG, STAT=ISTAT)
      IF (ISTAT .NE. 0) THEN
        ierr = cerror_dealloc
        if (temp_debug) WRITE(ERROR_UNIT, *) 'DEINIT_WORKSPACE_LSD:'//TRIM(CERROR_MSG(CERROR_DEALLOC))//':MSIG:ISTAT=',ISTAT
      ENDIF
    ENDIF

    IF (ALLOCATED(MWGT)) THEN
      DEALLOCATE(MWGT, STAT=ISTAT)
      IF (ISTAT .NE. 0) THEN
        ierr = cerror_dealloc
        if (temp_debug) WRITE(ERROR_UNIT, *) 'DEINIT_WORKSPACE_LSD:'//TRIM(CERROR_MSG(CERROR_DEALLOC))//':MWGT:ISTAT=',ISTAT
      ENDIF
    ENDIF
  END SUBROUTINE DEINIT_WORKSPACE_LSD

  !---------------------------------------------------------------------------  
  !> @author NRusomarov
  !> @date April, 2013
  !
  !> @brief Allocates the working arrays.
  !---------------------------------------------------------------------------  
  SUBROUTINE INIT_WORKSPACE_LSD(LINSIZ, LSDSIZ, SPSIZ, VSIZ,&
                               &WLCLIN, WGTLIN, WL, SP, SIGSP,&
                               &MSP, VLSD, PLSD, SIGLSD, MWGT, MSIG, ierr, debug)
    INTEGER(KIND=I4B), INTENT(IN) :: LINSIZ
    INTEGER(KIND=I4B), INTENT(IN) :: LSDSIZ
    INTEGER(KIND=I4B), INTENT(IN) :: SPSIZ
    INTEGER(KIND=I4B), INTENT(IN) :: VSIZ

    REAL(KIND=DP), ALLOCATABLE, INTENT(OUT) :: WLCLIN(:)
    REAL(KIND=DP), ALLOCATABLE, INTENT(OUT) :: WGTLIN(:,:)
    REAL(KIND=DP), ALLOCATABLE, INTENT(OUT) :: WL(:)
    REAL(KIND=DP), ALLOCATABLE, INTENT(OUT) :: SP(:)
    REAL(KIND=DP), ALLOCATABLE, INTENT(OUT) :: SIGSP(:)
    REAL(KIND=DP), ALLOCATABLE, INTENT(OUT) :: MSP(:)
    REAL(KIND=DP), ALLOCATABLE, INTENT(OUT) :: VLSD(:)
    REAL(KIND=DP), ALLOCATABLE, INTENT(OUT) :: PLSD(:)
    REAL(KIND=DP), ALLOCATABLE, INTENT(OUT) :: SIGLSD(:)
    REAL(KIND=DP), ALLOCATABLE, INTENT(OUT) :: MWGT(:)
    REAL(KIND=DP), ALLOCATABLE, INTENT(OUT) :: MSIG(:)

    INTEGER(KIND=I4B), intent(out) :: ierr
    logical, intent(in), optional :: debug

    !TMP VARS
    INTEGER(KIND=I4B) :: ISTAT
    logical :: temp_debug

    ierr = 0
    
    !set the debug flag
    temp_debug = .false.
    if (present(debug)) temp_debug = debug


    IF (.NOT. ALLOCATED(WLCLIN)) THEN
      ALLOCATE(WLCLIN(LINSIZ), STAT=ISTAT)
      IF (ISTAT .NE. 0) THEN
        ierr = cerror_alloc
        if (temp_debug) WRITE(ERROR_UNIT, *) 'INIT_WORKSPACE_LSD:'//TRIM(CERROR_MSG(ierr))//':WLCLIN:ISTAT=',ISTAT
      ENDIF
    ENDIF

    IF (.NOT. ALLOCATED(WGTLIN)) THEN
      ALLOCATE(WGTLIN(LSDSIZ,LINSIZ), STAT=ISTAT)
      IF (ISTAT .NE. 0) THEN
        ierr = cerror_alloc
        if (temp_debug) WRITE(ERROR_UNIT, *) 'INIT_WORKSPACE_LSD:'//TRIM(CERROR_MSG(ierr))//':WGTLIN:ISTAT=',ISTAT
      ENDIF
    ENDIF
    
    IF (.NOT. ALLOCATED(WL)) THEN
      ALLOCATE(WL(SPSIZ), STAT=ISTAT)
      IF (ISTAT .NE. 0) THEN
        ierr = cerror_alloc
        if (temp_debug) WRITE(ERROR_UNIT, *) 'INIT_WORKSPACE_LSD:'//TRIM(CERROR_MSG(ierr))//':WL:ISTAT=',ISTAT
      ENDIF
    ENDIF
    
    IF (.NOT. ALLOCATED(SP)) THEN
      ALLOCATE(SP(SPSIZ), STAT=ISTAT)
      IF (ISTAT .NE. 0) THEN
        ierr = cerror_alloc
        if (temp_debug) WRITE(ERROR_UNIT, *) 'INIT_WORKSPACE_LSD:'//TRIM(CERROR_MSG(ierr))//':SP:ISTAT=',ISTAT
      ENDIF
    ENDIF
    
    IF (.NOT. ALLOCATED(SIGSP)) THEN
      ALLOCATE(SIGSP(SPSIZ), STAT=ISTAT)
      IF (ISTAT .NE. 0) THEN
        ierr = cerror_alloc
        if (temp_debug) WRITE(ERROR_UNIT, *) 'INIT_WORKSPACE_LSD:'//TRIM(CERROR_MSG(ierr))//':SIGSP:ISTAT=',ISTAT
      ENDIF
    ENDIF
    
    IF (.NOT. ALLOCATED(MSP)) THEN
      ALLOCATE(MSP(SPSIZ), STAT=ISTAT)
      IF (ISTAT .NE. 0) THEN
        ierr = cerror_alloc
        if (temp_debug) WRITE(ERROR_UNIT, *) 'INIT_WORKSPACE_LSD:'//TRIM(CERROR_MSG(ierr))//':MSP:ISTAT=',ISTAT
      ENDIF
    ENDIF
    
    IF (.NOT. ALLOCATED(VLSD)) THEN
      ALLOCATE(VLSD(VSIZ), STAT=ISTAT)
      IF (ISTAT .NE. 0) THEN
        ierr = cerror_alloc
        if (temp_debug) WRITE(ERROR_UNIT, *) 'INIT_WORKSPACE_LSD:'//TRIM(CERROR_MSG(ierr))//':VLSD:ISTAT=',ISTAT
      ENDIF
    ENDIF

    IF (.NOT. ALLOCATED(PLSD)) THEN
      ALLOCATE(PLSD(VSIZ*LSDSIZ), STAT=ISTAT)
      IF (ISTAT .NE. 0) THEN
        ierr = cerror_alloc
        if (temp_debug) WRITE(ERROR_UNIT, *) 'INIT_WORKSPACE_LSD:'//TRIM(CERROR_MSG(ierr))//':PLSD:ISTAT=',ISTAT
      ENDIF
    ENDIF
    
    IF (.NOT. ALLOCATED(SIGLSD)) THEN
      ALLOCATE(SIGLSD(VSIZ*LSDSIZ), STAT=ISTAT)
      IF (ISTAT .NE. 0) THEN
        ierr = cerror_alloc
        if (temp_debug) WRITE(ERROR_UNIT, *) 'INIT_WORKSPACE_LSD:'//TRIM(CERROR_MSG(ierr))//':SIGLSD:ISTAT=',ISTAT
      ENDIF
    ENDIF
    
    IF (.NOT. ALLOCATED(MWGT)) THEN
      ALLOCATE(MWGT(LSDSIZ), STAT=ISTAT)
      IF (ISTAT .NE. 0) THEN
        ierr = cerror_alloc
        if (temp_debug) WRITE(ERROR_UNIT, *) 'INIT_WORKSPACE_LSD:'//TRIM(CERROR_MSG(ierr))//':MWGT:ISTAT=',ISTAT
      ENDIF
    ENDIF

    IF (.NOT. ALLOCATED(MSIG)) THEN
      ALLOCATE(MSIG(LINSIZ), STAT=ISTAT)
      IF (ISTAT .NE. 0) THEN
        ierr = cerror_alloc
        if (temp_debug) WRITE(ERROR_UNIT, *) 'INIT_WORKSPACE_LSD:'//TRIM(CERROR_MSG(ierr))//':MSIG:ISTAT=',ISTAT
      ENDIF
    ENDIF
  END SUBROUTINE INIT_WORKSPACE_LSD

end module lsd4_data