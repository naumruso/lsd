!------------------------------------------------------------------------------
! Uppsala University, Department of Physics and Astronomy, 
! Least-Squares Deconvolution Code
!------------------------------------------------------------------------------
!
!> @author NRusomarov
!> @date March, 2013
!
!> @brief I/O subroutines for the LSD standalone programs.
!
!> @todo Return cerror codes when errors encountered.
!------------------------------------------------------------------------------
MODULE LSD4_IO
  USE CDATA, ONLY : I4B, DP, IOMSG_LEN
  USE CERROR
  USE, INTRINSIC :: ISO_FORTRAN_ENV
  IMPLICIT NONE


  PRIVATE

  PUBLIC :: GET_DIMS_LSD,&
            INPUT_LSD,&
            OUTPUT_LSD,&
            COMPRFUNC

  CONTAINS

  !---------------------------------------------------------------------------
  !> @author Piskunov
  !
  !> @details
  !> This function elliminates spaces inside string 'name'.
  !> all characters other than spaces are moved to the left.
  !> A new string is padded to the right with spaces.
  !---------------------------------------------------------------------------
  FUNCTION COMPRFUNC(NAM) RESULT(NEW_NAME)
    CHARACTER(LEN=IOMSG_LEN), INTENT(IN) :: NAM
    CHARACTER(LEN=IOMSG_LEN)  :: NEW_NAME

    !TMP VARS
    INTEGER(KIND=I4B) :: L1, L2
    INTEGER(KIND=I4B) :: I, J

    L1=LEN(TRIM(NAM))
    L2=LEN(TRIM(NEW_NAME))

    J=1
    DO I=1, L1
      IF(NAM(I:I).NE.' '.AND.J.LE.L2) THEN
          NEW_NAME(J:J) = NAM(I:I)
          J=J+1
      END IF
    ENDDO

    IF(J.LE.L2) NEW_NAME(J:L2)=' '
  END FUNCTION COMPRFUNC

  !---------------------------------------------------------------------------  
  !> @author NRusomarov
  !> @date April, 2013
  !
  !> @brief Find the number of rows in a file.
  !---------------------------------------------------------------------------  
  SUBROUTINE READ_NROW(IUNIT, NROW, IERR)
    INTEGER(KIND=I4B), INTENT(IN) :: IUNIT

    INTEGER(KIND=I4B), INTENT(OUT) :: NROW
    INTEGER(KIND=I4B), INTENT(OUT) :: IERR


    NROW = 0
    DO
      READ(UNIT=IUNIT, FMT=*, IOSTAT=IERR)
      !check the error code and exit gracefully
      IF (IERR .NE. 0) THEN
        !End-of-file. This is expected behavior.
        if (ierr .lt. 0) ierr = 0
        !Some problem reading the file. Error.
        if (ierr .gt. 0) ierr = cerror_io_readline
        EXIT
      ENDIF
      NROW = NROW + 1
    ENDDO

    REWIND(UNIT=IUNIT, IOSTAT=IERR)
  END SUBROUTINE READ_NROW

  !---------------------------------------------------------------------------  
  !> @author NRusomarov
  !> @date April, 2013
  !
  !> @brief Find the maximum number of columns in a table file.
  !---------------------------------------------------------------------------  
  SUBROUTINE READ_NCOL (IUNIT, NCOL, IERR)
    INTEGER(KIND=I4B), INTENT(IN) :: IUNIT

    INTEGER(KIND=I4B), INTENT(OUT) :: NCOL
    INTEGER(KIND=I4B), INTENT(OUT) :: IERR

    !TMP VARS
    CHARACTER(LEN=1024) :: LINE
    CHARACTER(LEN=1024) :: IERR_TXT
    INTEGER(KIND=I4B) :: NROW
    INTEGER(KIND=I4B) :: NCOL_TMP

    INTEGER(KIND=I4B) :: I, J
    REAL(KIND=DP) :: ELEM

    !INIT INTENT(OUT) VARS
    NCOL = -1

    !GET TOTAL NUMBER OF LINES
    CALL READ_NROW(IUNIT, NROW, IERR)
    IF (IERR .ne. 0) RETURN

    DO I=1, NROW
      READ(UNIT=IUNIT, FMT='(A)', IOSTAT=IERR) LINE
      IF (IERR .LT. 0) THEN !EOF. NOTHING MORE TO INPUT. EXIT LOOP.
        IERR = 0
        EXIT
      ELSE IF (IERR .GT. 0) THEN !ERROR IN INPUT. BAD!
        IERR = cerror_io_readline
        RETURN
      ENDIF

      !GET NUMBER OF ELEMENTS ON THE CURRENT LINE
      NCOL_TMP = 1
      DO
        READ(LINE, FMT=*, IOSTAT=IERR) (ELEM, J=1, NCOL_TMP)
        IF (IERR .EQ. 0) THEN
          NCOL_TMP = NCOL_TMP + 1
        ELSE
          !THE FINAL VALUE OF NCOL_TMP RAISED ERROR
          NCOL_TMP = NCOL_TMP - 1
          EXIT
        ENDIF
      ENDDO
      
      !CHECK NUMBER OF ELEMENTS ON THE CURRENT LINE
      !since we are reading number of column and rows of a matrix
      !we expect that each line contains the same number of elements
      !If that is not true then this is not a matrix and
      !error code should be returned
      if (ncol < 0) then !first line
        ncol = ncol_tmp
      else if (ncol .ne. ncol_tmp) then
       ierr = cerror_io_num_cols_amb
       return
      endif
    ENDDO

    REWIND(UNIT=IUNIT, IOSTAT=IERR)
  END SUBROUTINE READ_NCOL

  !---------------------------------------------------------------------------
  !> @author NRusomarov
  !> @date April, 2013
  !
  !> @details This subroutine is used in conjunction with the standalone programs
  !> lsd4 and lsd4s. We supply the unit number of the file with the program parametes
  !> to the subroutine. It reads the only first two lines with the filenames of
  !> the observations matrix (WL, SP, SIGSP) and the LSD line mask (WLCLIN, WGTLIN).
  !> Once it knows the filenames it gets the sizes of the arrays (LINSIZ, LSDSIZ, SPSIZ)
  !> directly from the files, so that we don't have to supply that information.
  !> Furthermore, it also returns the size of the velocity grid for the LSD profiles.
  !---------------------------------------------------------------------------
  SUBROUTINE GET_DIMS_LSD(IUNIT, LINSIZ, LSDSIZ, SPSIZ, VSIZ, IERR, debug)
    INTEGER(KIND=I4B), INTENT(IN) :: IUNIT !< Unit number of the opened file.

    INTEGER(KIND=I4B), INTENT(OUT) :: LINSIZ !< Number of spectral lines (nrow)
    INTEGER(KIND=I4B), INTENT(OUT) :: LSDSIZ !< Number of LSD profiles (ncol)
    INTEGER(KIND=I4B), INTENT(OUT) :: SPSIZ !< Number of spectral points in observations.
    INTEGER(KIND=I4B), INTENT(OUT) :: VSIZ !< Size of velocity grid for the LSD profiles.

    INTEGER(KIND=I4B), INTENT(OUT) :: IERR !< Error code.
    logical, intent(in), optional :: debug !< debug flag


    !TMP VARS
    INTEGER(KIND=I4B) :: NCOL
    INTEGER(KIND=I4B) :: IUNIT_SP
    INTEGER(KIND=I4B) :: IUNIT_LIN
    CHARACTER(LEN=IOMSG_LEN) :: SPFILE
    CHARACTER(LEN=IOMSG_LEN) :: LINFILE
    REAL(KIND=DP) :: VFIRST
    REAL(KIND=DP) :: VLAST
    REAL(KIND=DP) :: VSTEP
    logical :: temp_debug

    !INIT INTENT(OUT) VARS
    LINSIZ = -1_i4b
    LSDSIZ = -1_i4b
    SPSIZ = -1_i4b
    VSIZ = -1_i4b

    temp_debug = .false.
    if (present(debug)) temp_debug = debug

    !READ FILENAME WITH OBSERVED SPECTRUM
    READ(IUNIT,*,IOSTAT=IERR) SPFILE
    IF (IERR < 0) THEN
      IERR = CERROR_io_EOF
      if (temp_debug) WRITE(ERROR_UNIT, *) 'GET_DIMS_LSD:'//TRIM(CERROR_MSG(IERR))//':OBSERVED SPECTRUM:CONFIG FILE'
      return
    ELSE IF (IERR > 0) THEN
      ierr = cerror_io_filename
      if (temp_debug) WRITE(ERROR_UNIT, *) 'GET_DIMS_LSD:'//TRIM(CERROR_MSG(IERR))//':OBSERVED SPECTRUM:CONFIG FILE'
      return
    ENDIF
    SPFILE=COMPRFUNC(SPFILE)

    !GET DIMENSIONS OF THE OBSERVATIONS MATRIX (WL, SP, SIGSP)
    OPEN(NEWUNIT=IUNIT_SP, FILE=TRIM(SPFILE), IOSTAT=IERR)
    IF (IERR .NE. 0) THEN
      ierr = cerror_io_openfile
      if (temp_debug) WRITE(ERROR_UNIT, *) 'GET_DIMS_LSD:'//TRIM(CERROR_MSG(IERR))//':'//TRIM(SPFILE)
      return
    ENDIF

    !GET NUMBER OF SPECTRAL POINTS
    CALL READ_NROW(IUNIT_SP, SPSIZ, IERR)
    IF (IERR .ne. 0) THEN
      if (temp_debug) WRITE(ERROR_UNIT, *) 'GET_DIMS_LSD:READ_NROW:'//TRIM(CERROR_MSG(IERR))//':'//TRIM(SPFILE)
      return
    ENDIF
    
    !GET NUMBER OF COLUMNS
    CALL READ_NCOL(IUNIT_SP, NCOL, IERR)
    IF (IERR .ne. 0) THEN
      if (temp_debug) WRITE(ERROR_UNIT, *) 'GET_DIMS_LSD:READ_NCOL:'//TRIM(CERROR_MSG(IERR))//':'//TRIM(SPFILE)
      return
    ENDIF
    
    !CHECK IF WE HAVE AT LEAST TWO COLUMNS IN THE OBSERVATIONS FILE
    IF (NCOL .LT. 2) THEN
      IERR = cerror_io_bad_obsdata
      if (temp_debug) WRITE(ERROR_UNIT, *) 'GET_DIMS_LSD:'//TRIM(CERROR_MSG(IERR))//':'//TRIM(SPFILE)
      return
    ENDIF


    !READ FILENAME WITH LINE LIST
    READ(IUNIT,*,IOSTAT=IERR) LINFILE
    IF (IERR < 0) THEN
      IERR = CERROR_io_eof
      if (temp_debug) WRITE(ERROR_UNIT, *) 'GET_DIMS_LSD:'//TRIM(CERROR_MSG(IERR))//':LINE LIST:CONFIG FILE'
      return
    ELSE IF (IERR > 0) THEN
      ierr = CERROR_io_filename
      if (temp_debug) WRITE(ERROR_UNIT, *) 'GET_DIMS_LSD:'//TRIM(CERROR_MSG(ierr))//':LINE LIST:CONFIG FILE'
      return
    ENDIF
    LINFILE=COMPRFUNC(LINFILE)

    !GET DIMENSIONS OF THE LINE LIST FILE
    OPEN(NEWUNIT=IUNIT_LIN, FILE=TRIM(LINFILE), IOSTAT=IERR)
    IF (IERR .NE. 0) THEN
      ierr = cerror_io_openfile
      if (temp_debug) WRITE(ERROR_UNIT, *) 'GET_DIMS_LSD:'//TRIM(CERROR_MSG(IERR))//':'//TRIM(LINFILE)
      return
    ENDIF

    !GET NUMBER OF SPECTRAL POINTS
    CALL READ_NROW(IUNIT_LIN, LINSIZ, IERR)
    IF (IERR .ne. 0) THEN
      if (temp_debug) WRITE(ERROR_UNIT, *) 'GET_DIMS_LSD:READ_NROW:'//TRIM(CERROR_MSG(IERR))//':'//TRIM(LINFILE)
      return
    ENDIF

    !GET NUMBER OF COLUMNS
    CALL READ_NCOL(IUNIT_LIN, NCOL, IERR)
    IF (IERR .ne. 0) THEN
      if (temp_debug) WRITE(ERROR_UNIT, *) 'GET_DIMS_LSD:READ_NCOL:'//TRIM(CERROR_MSG(IERR))//':'//TRIM(LINFILE)
      return
    ENDIF
    IF (NCOL .LT. 2) THEN
      ierr = cerror_io_bad_linelist
      if (temp_debug) WRITE(ERROR_UNIT, *) 'GET_DIMS_LSD:'//TRIM(CERROR_MSG(IERR))//':'//TRIM(LINFILE)
      return
    ENDIF
    LSDSIZ = NCOL - 1


    !READ VELOCITY LIMITS AND STEP
    READ(IUNIT,*,IOSTAT=IERR) VFIRST,VLAST,VSTEP
    IF (IERR < 0) THEN
      ierr = cerror_io_eof
      if (temp_debug) WRITE(ERROR_UNIT, *) 'GET_DIMS_LSD:'//TRIM(CERROR_MSG(ierr))//':VELOCITY LIMITS:CONFIG FILE'
      return
    ELSE IF (IERR > 0) THEN
      ierr = cerror_io_format_velocity
      if (temp_debug) WRITE(ERROR_UNIT, *) 'GET_DIMS_LSD:'//TRIM(CERROR_MSG(ierr))//':CONFIG FILE'
      return
    ENDIF
    VSIZ=NINT((VLAST-VFIRST)/VSTEP)+1

    CLOSE(UNIT=IUNIT_SP)
    CLOSE(UNIT=IUNIT_LIN)
    REWIND(UNIT=IUNIT)
  END SUBROUTINE GET_DIMS_LSD


  !---------------------------------------------------------------------------
  !> @author NRusomarov
  !> @date April, 2013
  !
  !> @brief Input all data from the data files.
  !---------------------------------------------------------------------------
  SUBROUTINE INPUT_LSD(IUNIT,&
                      &LINSIZ, LSDSIZ, SPSIZ,&
                      &WLCLIN, WGTLIN, WL, SP, SIGSP,&
                      &REG, POL,&
                      &VFIRST, VLAST, VSTEP,&
                      &IERR, debug)
    INTEGER(KIND=I4B), INTENT(IN) :: IUNIT !< Unit number of the lsd4 parameters file

    INTEGER(KIND=I4B), INTENT(IN) :: LINSIZ !< Number of sp. lines
    INTEGER(KIND=I4B), INTENT(IN) :: LSDSIZ !< Number of LSD profiles
    INTEGER(KIND=I4B), INTENT(IN) :: SPSIZ  !< Number of pixels from observations

    REAL(KIND=DP), INTENT(OUT) :: WLCLIN(LINSIZ)          !< arrays with central wavelengths of sp. lines
    REAL(KIND=DP), INTENT(OUT) :: WGTLIN(LSDSIZ, LINSIZ)  !< matrix with LSd weights
    REAL(KIND=DP), INTENT(OUT) :: WL(SPSIZ)               !< wavelengths array (observations)
    REAL(KIND=DP), INTENT(OUT) :: SP(SPSIZ)               !< spectrum array (observations)
    REAL(KIND=DP), INTENT(OUT) :: SIGSP(SPSIZ)            !< errors/weights for spectrum array (observations)

    REAL(KIND=DP), INTENT(OUT) :: REG                     !< regularization parameter
    INTEGER(KIND=I4B), INTENT(OUT) :: POL                 !< polarization parameter

    REAL(KIND=DP), INTENT(OUT) :: VFIRST                  !< value of first velocity point
    REAL(KIND=DP), INTENT(OUT) :: VLAST                   !< value of last velocity points
    REAL(KIND=DP), INTENT(OUT) :: VSTEP                   !< velocity step

    INTEGER(KIND=I4B), INTENT(OUT) :: IERR                !< error code
    logical, intent(in), optional :: debug !< debug flag


    !TMP VARS
    INTEGER(KIND=I4B) :: ISTAT
    CHARACTER(LEN=IOMSG_LEN) :: SPFILE
    CHARACTER(LEN=IOMSG_LEN) :: LINFILE
    INTEGER(KIND=I4B) :: NCOL, NWL, NLIN, NLSD
    INTEGER(KIND=I4B) :: ILSD
    logical :: temp_debug

    !ALLOCATABLE
    REAL(KIND=DP), ALLOCATABLE :: DAT(:,:)

    !set the debug flag
    temp_debug = .false.
    if (present(debug)) temp_debug = debug

    !INIT INTENT(OUT) VARS
    WLCLIN = 0_dp
    WGTLIN = 0_dp
    WL = 0_dp
    SP = 0_dp
    SIGSP = 0_dp
    REG = 0_dp
    POL = 0_i4b
    VFIRST = 0_dp
    VLAST = 0_dp
    VSTEP = 0_dp
    IERR = 0_i4b


    !READ FILENAME WITH OBSERVED SPECTRUM
    READ(IUNIT,*,IOSTAT=IERR) SPFILE
    IF (IERR < 0) THEN
      IERR = CERROR_io_EOF
      if (temp_debug) WRITE(ERROR_UNIT, *) 'INPUT_LSD:'//TRIM(CERROR_MSG(IERR))//':OBSERVED SPECTRUM:CONFIG FILE'
      return
    ELSE IF (IERR > 0) THEN
      ierr = cerror_io_filename
      if (temp_debug) WRITE(ERROR_UNIT, *) 'INPUT_LSD:'//TRIM(CERROR_MSG(IERR))//':OBSERVED SPECTRUM:CONFIG FILE'
      return
    ENDIF
    SPFILE=COMPRFUNC(SPFILE)

    !ALLOCATING ARRAY DAT FOR READING THE OBS. SPECTRUM
    ALLOCATE(DAT(3,SPSIZ), STAT=ISTAT)
    IF (ISTAT .NE. 0) THEN
      ierr = cerror_alloc
      if (temp_debug) WRITE(ERROR_UNIT, *) 'INPUT_LSD:'//TRIM(CERROR_MSG(ierr))//':'//TRIM(SPFILE)
      return
    ENDIF

    !READ OBSERVED SPECTRUM, INVERTING ERROR BAR
    CALL RDCLM_LSD(SPFILE,3,SPSIZ,DAT,NCOL,NWL,ierr)
    if (ierr .ne. 0) then
      if (temp_debug) WRITE(ERROR_UNIT, *) 'INPUT_LSD:RDCLM_LSD:'//TRIM(CERROR_MSG(ierr))//':'//TRIM(SPFILE)
      return
    endif
    !CHECK THAT SIZES MATCH
    IF (NWL .NE. SPSIZ) THEN
      ierr = cerror_io_bad_obsdata
      if (temp_debug) WRITE(ERROR_UNIT, *) 'INPUT_LSD:'//TRIM(CERROR_MSG(ierr))//':INCONSISTENT NUMBER OF'//&
                            &' WAVELENGTH POINTS IN OBSERVATIONS FILE:', NWL, SPSIZ
      return
    ENDIF
    !COPY DATA
    WL=DAT(1,:)
    SP=DAT(2,:)
    IF(NCOL.EQ.3) THEN 
      SIGSP=1_DP/DAT(3,:)
    ELSE
      SIGSP=1_DP
    END IF

    !DEALLOC THE DAT ARRAY
    DEALLOCATE(DAT, STAT=ISTAT)
    IF (ISTAT .NE. 0) THEN
      ierr = cerror_dealloc
      if (temp_debug) WRITE(ERROR_UNIT, *) 'INPUT_LSD:'//TRIM(CERROR_MSG(ierr))//':'//TRIM(SPFILE)
      return
    ENDIF

    !READ FILENAME WITH LINE LIST
    READ(IUNIT,*,IOSTAT=IERR) LINFILE
    IF (IERR < 0) THEN
      IERR = CERROR_io_EOF
      if (temp_debug) WRITE(ERROR_UNIT, *) 'INPUT_LSD:'//TRIM(CERROR_MSG(IERR))//':LINE LIST:CONFIG FILE'
      return
    ELSE IF (IERR > 0) THEN
      ierr = cerror_io_filename
      if (temp_debug) WRITE(ERROR_UNIT, *) 'INPUT_LSD:'//TRIM(CERROR_MSG(IERR))//':LINE LIST:CONFIG FILE'
      return
    ENDIF
    LINFILE=COMPRFUNC(LINFILE)

    !ALLOCATING THE DAT FOR READING THE LINE LIST
    ALLOCATE(DAT(1+LSDSIZ,LINSIZ), STAT=ISTAT)
    IF (ISTAT .NE. 0) THEN
      ierr = cerror_alloc
      if (temp_debug) WRITE(ERROR_UNIT, *) 'INPUT_LSD:'//TRIM(CERROR_MSG(ierr))//':'//TRIM(LINFILE)
      return
    ENDIF

    !READ LINE LIST
    CALL RDCLM_LSD(LINFILE,1+LSDSIZ,LINSIZ,DAT,NCOL,NLIN,ierr)
    if (ierr .ne. 0) then
      if (temp_debug) WRITE(ERROR_UNIT, *) 'INPUT_LSD:RDCLM_LSD:'//TRIM(CERROR_MSG(ierr))//':'//TRIM(LINFILE)
      return
    endif

    !CHECK IF SIZES MATCH
    IF(NLIN .NE. LINSIZ) THEN
      ierr = cerror_io_bad_linelist
      if (temp_debug) WRITE(ERROR_UNIT, *) 'INPUT_LSD:'//TRIM(CERROR_MSG(ierr))&
                                          &//':INCONSISTENT NUMBER OF SPECTRAL LINES',nlin,linsiz
      return
    END IF

    !CHECK IF WE ALSO GET THE RIGHT NUMBER OF COLUMNS, I.E., NUMBER OF LSD PROFILES
    NLSD=NCOL-1
    IF (NLSD .NE. LSDSIZ) THEN
      ierr = cerror_io_num_cols_amb
      if (temp_debug) WRITE(ERROR_UNIT, *) 'INPUT_LSD:'//TRIM(CERROR_MSG(ierr))//':INCONSISTENT NUMBER OF LSD PROFILES',nlsd,lsdsiz
      return
    END IF

    !COPY DATA
    WLCLIN=DAT(1,:)
    DO ILSD=1,NLSD
      WGTLIN(ILSD,:)=DAT(ILSD+1,:)
    ENDDO

    !DEALLOC THE DAT ARRAY
    DEALLOCATE(DAT, STAT=ISTAT)
    IF (ISTAT .NE. 0) THEN
      ierr = cerror_dealloc
      if (temp_debug) WRITE(ERROR_UNIT, *) 'INPUT_LSD:'//TRIM(CERROR_MSG(ierr))//':'//TRIM(LINFILE)
      return
    ENDIF


    !READ VELOCITY LIMITS AND STEP
    READ(IUNIT,*,IOSTAT=IERR) VFIRST,VLAST,VSTEP
    IF (IERR < 0) THEN
      ierr = cerror_io_eof
      if (temp_debug) WRITE(ERROR_UNIT, *) 'INPUT_LSD:'//TRIM(CERROR_MSG(ierr))//':VELOCITY LIMITS:CONFIG FILE'
      return
    ELSE IF (IERR > 0) THEN
      ierr = cerror_io_format_velocity
      if (temp_debug) WRITE(ERROR_UNIT, *) 'INPUT_LSD:'//TRIM(CERROR_MSG(ierr))//':CONFIG FILE'
      return
    ENDIF


    !READ POLARIZATION FLAG
    READ(IUNIT,*,IOSTAT=IERR) POL
    IF (IERR < 0) THEN
      ierr = cerror_io_eof
      if (temp_debug) WRITE(ERROR_UNIT, *) 'INPUT_LSD:'//TRIM(CERROR_MSG(ierr))//':POLARIZATION FLAG:CONFIG FILE'
      return
    ELSE IF (IERR > 0) THEN
      ierr = CERROR_io_FORMAT_POL_FLAG
      if (temp_debug) WRITE(ERROR_UNIT, *) 'INPUT_LSD:'//TRIM(CERROR_MSG(ierr))//':CONFIG FILE'
      return
    ENDIF


    !READ OPTIONAL REGULARIZATION PARAMETER
    READ(IUNIT,*,IOSTAT=IERR) REG
    IF (IERR .lt. 0) THEN  !-1 means nothing was there, i.e., we set the default value for the reg param.
      REG = 0_DP
      ierr = 0_i4b
    ENDIF
  END SUBROUTINE INPUT_LSD

  !---------------------------------------------------------------------------  
  !> @author NRusomarov
  !> @date April, 2013
  !
  !> @brief Helper function for printing out results.
  !---------------------------------------------------------------------------  
  SUBROUTINE OUTPUT_LSD(LSD_FOUNIT, MOD_FOUNIT, WLN_FOUNIT,&
                       &VSIZ, LSDSIZ, SPSIZ, LINSIZ, NWL,&
                       &WL, MSP, SP, SIGSP,&
                       &WLCLIN, MSIG,&
                       &VLSD, PLSD, SIGLSD,&
                       &CHI, POL)
    INTEGER(KIND=I4B), INTENT(IN) :: LSD_FOUNIT !< Unit number of file for output of VLSD, PLSD, SIGLSD
    INTEGER(KIND=I4B), INTENT(IN) :: MOD_FOUNIT !< Unit number of file for output of WL, SP, MSP, SIGSP
    INTEGER(KIND=I4B), INTENT(IN) :: WLN_FOUNIT !< Unit number of file for output of WLCLIN, MSIG

    INTEGER(KIND=I4B), INTENT(IN) :: VSIZ   !< velocity grid size
    INTEGER(KIND=I4B), INTENT(IN) :: LSDSIZ !< num. of LSD profiles
    INTEGER(KIND=I4B), INTENT(IN) :: SPSIZ  !< size of observed spectrum
    INTEGER(KIND=I4B), INTENT(IN) :: LINSIZ !< number of spectral lines
    INTEGER(KIND=I4B), INTENT(IN) :: NWL    !< number of used wavelength points from SP

    REAL(KIND=DP), INTENT(IN) :: WL(SPSIZ)     !< wavelength array for observations
    REAL(KIND=DP), INTENT(IN) :: MSP(SPSIZ)    !< modeled spectrum
    REAL(KIND=DP), INTENT(IN) :: SP(SPSIZ)     !< observed spectrum
    REAL(KIND=DP), INTENT(IN) :: SIGSP(SPSIZ)  !< sigma errors for observations

    REAL(KIND=DP), INTENT(IN) :: WLCLIN(LINSIZ) !< central wavelengths for sp. lines used in the calculations of the LSD profiles
    REAL(KIND=DP), INTENT(IN) :: MSIG(LINSIZ)   !< line weights array after computation of the line-pattern matrix MM

    REAL(KIND=DP), INTENT(IN) :: VLSD(VSIZ)        !< velocity space
    REAL(KIND=DP), INTENT(IN) :: PLSD(VSIZ*LSDSIZ) !< LSD profiles
    REAL(KIND=DP), INTENT(IN) :: SIGLSD(VSIZ*LSDSIZ) !< sigma error for the LSd profiles

    REAL(KIND=DP), INTENT(IN) :: CHI                 !< Reduced chi^2
    INTEGER(KIND=I4B), INTENT(IN) :: POL             !< polarization value

    ! LOCAL VARS
    REAL(KIND=DP) :: SN
    INTEGER(KIND=I4B) :: I, J, IWL, ILINE
    INTEGER(KIND=I4B) :: I0


    ! PRINT SIZES
    WRITE(LSD_FOUNIT,'(I6,I6,$)') VSIZ,LSDSIZ

    ! OUTPUT LSD PROFILE
    DO I=1,LSDSIZ
      I0 = (I-1)*VSIZ

      ! COMPUTE AVERAGE S/N RATIO OF THE LSD PROFILES
      SN = VSIZ / SUM(SIGLSD(I0:I0 + VSIZ))
      WRITE(LSD_FOUNIT,'(I6,ES11.4)') NINT(SN),CHI

      DO J=1,VSIZ
        WRITE(LSD_FOUNIT,'(F8.2,2ES14.5)') VLSD(J),PLSD(J+I0),SIGLSD(J+I0)
      ENDDO
    ENDDO

    ! WRITE MODEL SPECTRUM
    DO IWL=1,NWL
      IF(POL.EQ.0) THEN
        WRITE(MOD_FOUNIT,'(F12.6,3E14.5)') WL(IWL),1_DP-MSP(IWL),1_DP-SP(IWL),1_DP/SIGSP(IWL)
      ELSE
        WRITE(MOD_FOUNIT,'(F12.6,3E14.5)') WL(IWL),MSP(IWL),SP(IWL),1_DP/SIGSP(IWL)
      ENDIF
    ENDDO

    ! OUTPUT THE LINE WEIGHTS
    DO ILINE=1, LINSIZ
      WRITE(WLN_FOUNIT, '(F11.4,1x,ES11.4)') WLCLIN(ILINE), MSIG(ILINE)
    ENDDO
  END SUBROUTINE OUTPUT_LSD


  SUBROUTINE RDCLM_LSD(DFILE,NCOLMAX,NROWMAX,DAT,NCOL,NROW,IERR)
    CHARACTER(LEN=IOMSG_LEN), INTENT(IN) :: DFILE
    INTEGER(KIND=I4B), INTENT(IN) :: NCOLMAX, NROWMAX
    REAL(KIND=DP), INTENT(INOUT) :: DAT(NCOLMAX,NROWMAX)
    INTEGER(KIND=I4B), INTENT(OUT) :: NCOL, NROW
    INTEGER(KIND=I4B), INTENT(OUT) :: IERR

    !TMP VARS
    CHARACTER(LEN=1024) :: SLINE
    INTEGER(KIND=I4B) :: IUNIT
    INTEGER(KIND=I4B) :: I, J, L

    !INIT INTENT(OUT) VARS
    NCOL = -1
    NROW = -1

    ! OPEN FILE
    OPEN(NEWUNIT=IUNIT,FILE=TRIM(DFILE),STATUS='OLD',IOSTAT=IERR)
    IF(IERR.NE.0) THEN
      ierr = cerror_io_openfile
      return
    END IF

    ! READ FIRST LINE
    READ(IUNIT,'(A)',IOSTAT=IERR) SLINE
    IF (IERR < 0) THEN
      ierr = cerror_io_eof
      return
    ELSE IF (IERR > 0) THEN
      ierr = cerror_io_readline
      return
    ENDIF

    ! COUNT NUMBER OF TRANSITIONS *TO* WHITESPACE
    NCOL=0
    L=LEN(SLINE)
    DO I=2,L
      IF(SLINE(I:I).EQ.' '.AND.SLINE(I-1:I-1).NE.' ') NCOL=NCOL+1
    ENDDO
    IF(NCOL.GT.NCOLMAX) THEN 
      ierr = cerror_io_num_cols_amb
      return
    END IF
    REWIND(UNIT=IUNIT)

    ! READ DATA
    NROW=0
    DO WHILE (IERR .EQ. 0)
      NROW=NROW+1
      READ(IUNIT,*,IOSTAT=IERR) (DAT(J,NROW),J=1,NCOL)
    ENDDO
    NROW=NROW-1

    if (ierr .gt. 0) then
      ierr = cerror_io_readline
    else
      ierr = 0
    endif
    
    CLOSE(UNIT=IUNIT)

  END SUBROUTINE RDCLM_LSD
END MODULE LSD4_IO
