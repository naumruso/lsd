import cPickle
import asciidata
import numpy as np

from scipy.io import readsav
from pylsd import pylsd

pylsd._wlc = 'wl'
pylsd._wl = 'wl'
pylsd._sp = 'sx'


def get_pairs(spectra):
    spectra = [[spectrum, i] for spectrum, i in zip(spectra, range(len(spectra)))]

    pairs = []
    nspectra = len(spectra)
    for i in xrange(nspectra):
        for j in xrange(i+1, nspectra, 1):
            spec_i = spectra[i][0].replace('_r_', '_x_').replace('_b_', '_x_')
            spec_j = spectra[j][0].replace('_r_', '_x_').replace('_b_', '_x_')
            if spec_i == spec_j:
                if spectra[i][0][-7] == 'b':
                    pairs.append((i, j))
                else:
                    pairs.append((j, i))
                break
    return pairs


def gather_data(spectra):
    obs = {'wl': [], 'sx': [], 'gx': [], 'six': [], 'gix': [], 'nx': []}
    for spectrum in spectra:
        tmp = readsav(spectrum, verbose=False)
        obs['wl'] += tmp.wl.flatten().tolist()
        obs['sx'] += tmp.sx.flatten().tolist()
        obs['gx'] += tmp.gx.flatten().tolist()
        obs['six'] += tmp.six.flatten().tolist()
        obs['gix'] += tmp.gix.flatten().tolist()
        if isinstance(tmp.nx, np.ndarray) and obs['nx'] is not None:
            obs['nx'] += tmp.nx.flatten().tolist()
        else:
            obs['nx'] = None
    return obs


def STAR(spectra, lines, sp_type, **kwargs):
    """
    This is written specifically for the spectra that we get after full
    reduction of HARPSpol spectra, and for the line mask that is
    produced by the Oleg's IDL programs.

    **kwargs are passed to run_lsd.

    sp_type is obligatory.
    """

    # making new data
    obs = gather_data(spectra)

    # reading lines
    mask = readsav(lines, verbose=False)
    lines = mask['lines']

    # SX
    kwargs['sp_type'] = sp_type
    obs_sx = np.array(zip(obs['wl'], obs['sx'], obs['gx']),
                      dtype=[(pylsd._wl, np.float64),
                             (pylsd._sp, np.float64),
                             (pylsd._sig_sp, np.float64)])
    sx = pylsd.run_lsd(obs_sx, lines, **kwargs)

    # SIX
    kwargs['sp_type'] = 'i'
    obs_six = np.array(zip(obs['wl'], obs['six'], obs['gix']),
                       dtype=[(pylsd._wl, np.float64),
                              (pylsd._sp, np.float64),
                              (pylsd._sig_sp, np.float64)])
    six = pylsd.run_lsd(obs_six, lines, **kwargs)

    # NX
    if obs['nx'] is not None:
        kwargs['sp_type'] = sp_type
        obs_nx = np.array(zip(obs['wl'], obs['nx'], obs['gx']),
                          dtype=[(pylsd._wl, np.float64),
                                 (pylsd._sp, np.float64),
                                 (pylsd._sig_sp, np.float64)])
        nx = pylsd.run_lsd(obs_nx, lines, **kwargs)
    else:
        nx = None

    return {'sx': sx, 'six': six, 'nx': nx}
    return None


# program parameters
NUM_PROCESSES = 4
SMP = False  # use threading if set to True
verbose = not SMP  # depends on SMP

# LSD params
cutoff = 0.1
gaps = [[0.0, 3995.0],  # beginning, H lines up to Hepsilon
        [4072.0, 4132.0],  # Hdelta
        [4313.0, 4367.0],  # Hgamma
        [4836.0, 4886.0],  # Hbeta
        [5257.5, 5339.0],  # CCD gap
        [5880.0, 6000.0],  # telluric
        [6270.0, 6330.0],  # telluric
        [6382.5, 6387.5],  # defect
        [6460.0, 6590.0],  # telluric, Halpha (6562)
        [6860.0, 9000.0]]  # telluric, end
wl0 = 5000.0
qualt = False
species = {'REE': [['Fe', 'Cr', 'Co', 'Ti', 'Si', 'Ca',  'V', 'Mn', 'Ni',  'C',
                     'Y', 'Zr', 'Mg',  'S', 'Sc', 'Na',  'O', 'Sr', 'Ba', 'Al',
                    'Cu', 'Th', 'Hf', 'Zn', 'Mo', 'Ga', 'In', 'Nb', 'Pb',  'K',
                     'N',  'P',  'G', 'Ge'],
                   ['La', 'Ce', 'Pr', 'Nd', 'Sm', 'Eu', 'Gd', 'Tb', 'Dy', 'Ho',
                    'Er', 'Tm', 'Yb']],
           'Fe-peak':
                  [['Si', 'Ca',  'C',  'Y', 'Zr', 'Mg',  'S', 'Sc', 'Na',  'O',
                    'Sr', 'Ba', 'Al', 'Cu', 'Th', 'Hf', 'Zn', 'Mo', 'Ga', 'In',
                    'Nb', 'Pb',  'K',  'N',  'P',  'G', 'La', 'Ce', 'Pr', 'Nd',
                    'Sm', 'Eu', 'Gd', 'Tb', 'Dy', 'Ho', 'Er', 'Tm', 'Yb', 'Ge'],
                   ['Fe', 'Cr', 'Co', 'Ti',  'V', 'Mn', 'Ni']],
           'REE+Fe-peak':
                  [['Si', 'Ca',  'C',  'Y', 'Zr', 'Mg', 'S', 'Sc', 'Na', 'O',
                    'Sr', 'Ba', 'Al', 'Cu', 'Th', 'Hf', 'Zn', 'Mo', 'Ga', 'In',
                    'Nb', 'Pb',  'K',  'N',  'P',  'G', 'Ge'],
                   ['La', 'Ce', 'Pr', 'Nd', 'Sm', 'Eu', 'Gd', 'Tb', 'Dy', 'Ho',
                    'Er', 'Tm', 'Yb'],
                   ['Fe 1', 'Cr 1', 'Co 1', 'Ti 1', 'V 1', 'Mn 1', 'Ni 1']],
           'full': None}


# input data (lines and observations)
lines = '3780-6920.sav'
spectra = ['2010-01-03_HD-24712_b_q.sav',
           '2010-01-03_HD-24712_b_u.sav',
           '2010-01-03_HD-24712_b_v.sav',
           '2010-01-03_HD-24712_r_q.sav',
           '2010-01-03_HD-24712_r_u.sav',
           '2010-01-03_HD-24712_r_v.sav',
           '2010-01-04_HD-24712_b_q.sav',
           '2010-01-04_HD-24712_b_u.sav',
           '2010-01-04_HD-24712_b_v.sav',
           '2010-01-04_HD-24712_r_q.sav',
           '2010-01-04_HD-24712_r_u.sav',
           '2010-01-04_HD-24712_r_v.sav']


# combine the spectra into groups of two (red, blue)
pairs = get_pairs(spectra)


# create the input arguments list
# this goes through pairs and species
args = []
for skey in species.viewkeys():
    for pair in pairs:
        args.append([[spectra[pair[0]], spectra[pair[1]]], skey])


# for use with the multiprocessing module
# we declare it here, so that we can use the other variables
def STAR_WRAPPER(arg):
    if verbose:
        print('Start: '+arg[0][0])
        print('Start: '+arg[0][1])
    sp_type = arg[0][0][-5]
    res = STAR(arg[0], lines, sp_type,
               gaps=gaps, cutoff=cutoff, wl0=wl0, qualt=qualt,
               verbose=verbose, species=species[arg[1]])
    bname = arg[0][0][:-7]
    fname = bname+sp_type+'.'+arg[1]+'.pyb.gz'
    fout = open(fname, 'wb')
    cPickle.dump(res, fout)
    fout.close()
    if verbose:
        print('Done.')


# the stack size should be unlimited
# ulimit -s unlimited
if SMP:
    from multiprocessing import Pool
    p = Pool(NUM_PROCESSES)
    p.map(STAR_WRAPPER, args)
else:
    map(STAR_WRAPPER, args)
