import os
import subprocess

import setuptools
from numpy.distutils.core import setup, Extension

from pylsd import __version__ as version


# Preprocessing functions
def preprocessedfilename(infile):
    """
    Returns the preprocessed file name for the source file name
    passed in.
    """
    base, ext = os.path.splitext(infile)

    if ext == '.F':
        outfile = base + '.f'
    elif ext == '.F90':
        outfile = base + '.f90'
    else:
        outfile = infile

    return outfile


def preprocess(filein, outDir):
    """
    Preprocess the input files and save two versions of them.
    The first version is saved in the original directory.
    The second version is for f2py and is saved in ../pylsd.
    """
    filename = os.path.basename(filein)  # get filename
    srcDir = os.path.dirname(filein)  # get dirname

    outFile = preprocessedfilename(filename)
    fileout = os.path.join(outDir, outFile)

    cerror_datafile = os.path.join(srcDir, 'cerror_data.txt')

    # run the preprocessor
    return (0 == subprocess.call("expander.py --eval='pylsd=True;\
            cerror_datafile=\"%s\"' -f %s > %s" %
            (cerror_datafile, filein, fileout), shell=True))

Ffiles = ['cerror.F90', 'cdata.F90']  # files to be processed by F2PY.
ffiles = ['math.f', 'lsd_core.f90']  # okay files

outDir = 'pylsdf'  # output directory for the preprocessed files
srcDir = '../src'  # directory for files that do not need to be preprocessed

# add the files that do not need preprocessing
pyffiles = []
for ffile in ffiles:
    pyffiles += [os.path.join(srcDir, ffile)]

# preprocess and add the new files to pyffiles
for Ffile in Ffiles:
    preprocess(os.path.join(srcDir, Ffile), outDir)
    pyffiles = [os.path.join(outDir, preprocessedfilename(Ffile))] + pyffiles

# create the extensions for f2py
ext1 = Extension(name='_pylsdf', sources=pyffiles)
ext2 = Extension(name='_fmodules', sources=['pylsdf/cdata.f90',
                                            'pylsdf/fspeeders.f90'])


short_descr = "pylsd: python wrappers around the Fortran LSD code."

classifiers = [['Development Status :: 5 - Production/Stable',
               'Development Status :: 4 - Beta'][int('b' in version)],
               'Programming Language :: Python',
               'Intended Audience :: Science/Research',
               'Topic :: Scientific/Engineering :: Astronomy',
               'License :: OSI Approved :: GPLv3',
               'Operating System :: POSIX :: Linux', ]


def configuration(parent_package='', top_path=None, package_name='pylsd'):
    from numpy.distutils.misc_util import Configuration
    config = Configuration(package_name, parent_package, top_path,
                           version=version,
                           description=short_descr)
    config.add_data_files('tests/data/*')
    config.add_data_files('tests/test_*.py')

    return config


setup(configuration=configuration,
      install_requires='numpy',
      namespace_packages=['pylsd'],
      packages=setuptools.find_packages(),
      classifiers=classifiers,
      ext_modules=[ext1, ext2],
      include_package_data=True)
