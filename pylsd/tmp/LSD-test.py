import matplotlib.pyplot as plt
import numpy as np
from scipy import interpolate, optimize, weave, linalg
from scipy.weave import converters

c = 299792.458

def condfunc(x,y):
    return x and y
vcondfunc = np.vectorize(condfunc)

def select_line(wave,cwave,vstart,vend):
  """
  cwave is the laboratory wavelength of the 
  selected line
  returns the start and end indices of wave where the condition 
  wavestart < wave < waveend is satisfied
  
  input:
    wave: numpy 1d array [float]
    cwave: numpy 1d array [float]
    vstart: scalar [float]
    vend: scalar [float]
  
  output:
    wave_start: scalar [int]
    wave_end: scalar [int]
  """

  wavestart = cwave*(vstart/c + 1.0)
  waveend = cwave*(vend/c + 1.0)

  condlist_start = wave > wavestart
  condlist_end = wave < waveend
  condlist = vcondfunc(condlist_start,condlist_end)

  wave_indices = np.where(condlist)[0]
  wave_start = wave_indices[0]
  waev_end = wave_indices[-1]
  return wave_start, wave_end

def SpectrumSelection(wave,lines,vstart,vend):
  """
  wave is 1d array in which wave[i] < wave[i+1]
  returns 2d array with start and end indices for each line, where
  wavestart < wave < waveend is satisfied for each line.
  note: same as select_line except it works with more than one line
  
  input:
    wave: numpy 1d array [float]
    lines: numpy 1d array [float]
    vstart: scalar [float]
    vend: scalar [float]
  
  output:
    spectrum_selection: numpy 2d array [int]
  """
  
  spectrum_selection = []
  for line in lines:
    wave_indice_start,wave_indice_end = select_line(wave,line,vstart,vend)
    spectrum_selection.append([wave_indice_start,wave_indice_end])
  return np.array(spectrum_selection)

def NormalizationCoefficients(spectrum_selection, sig, cwave, geff, depth):
  """
  spectrum_selection is the 2d array from SpectrumSelection
  cwave, geff, depth are lists that contain central wavelengths, depths and lande factors for each line
  sig is 1d array that contains sigma for each pixel in the spectrum
  1) calculate sigma around each line using spectrum_selection and sigma (takes mean)
  2) use this data to calculate lfactor0, line0, depth0
  
  input:
    spectrum_selection: numpy 2d array [int]
    sig: numpy 1d array [float]
    cwave: numpy 1d array [float]
    geff: numpy 1d array [float]
    depth: numpy 1d array [float]
  
  output:
    cwave0: scalar [float]
    geff0: scalar [float]
    depth0: scalar [float]
  """

  sigma = []
  for cur_sel in spectrum_selection:
    sigma.append(np.mean(sig[cur_sel[0]:(cur_sel[1]+1)]))
  sigma = 1.0 / np.array(sigma, dtype=np.float64)**2.0
  sum_sigma = np.sum(sigma)

  cwave0 = np.sum(sigma*np.array(cwave))/sum_sigma
  geff0 = np.sum(sigma*np.array(geff))/sum_sigma
  depth0 = np.sum(sigma*np.array(depth))/sum_sigma
  return cwave0, geff0, depth0

def load_matrixR(R):
  """
  changes:
    R: numpy 2d array [float], created with numpy.zeros
  """
  
  nrow, ncol = R.shape

  #first row
  R[0, 0] = 1.0
  R[0, 1] = -1.0

  #last row
  R[-1, -1] = 1.0
  R[-1, -2] = -1.0
  
  #other rows
  for i in range(1,nrow-1,1):
    R[i, i-1] = -1.0
    R[i, i] = 2.0
    R[i, i+1] = -1.0

def MaskCreator(cwave, wave, vel_space, norm_coef = 1.0):
  """
  candidate for Cython optimization
  returns nonormalized mask if norm_coef is set to 1.0
  
  input:
    cwave: scalar [float]
    wave: numpy 1d array [float]
    vel_space: numpy 1d array [float]
    norm_coef: scalar [float], optional
  
  return:
    mask: numpy 2d array [float]
  """
  
  vstep = ((vel_space[1] - vel_space[0]) + (vel_space[-1] - vel_space[-2])) / 2.0

  coef = norm_coef / vstep
  vel_space_line = c * (wave - cwave) / cwave
  mask = np.zeros((vel_space_line.size, vel_space.size), dtype = np.float64)

  #indice1 = j
  #indice2 = j+1
  #we need to fill two elements in mask i.e. mask[indice1], mask[indice2]
  #where indice1 and indice2 follow from the following condition
  #vel_space[indice1] <= vel_space_line[i] <= vel_space_line[indice2]
  for j in range(vel_space_line.size):
    cond1 = vel_space <= vel_space_line[j]
    indice1 = np.where(cond1)[0][-1]
    indice2 = indice1 + 1
    mask[j][indice1] = (vel_space[indice2]-vel_space_line[j]) * coef
    mask[j][indice2] = (vel_space_line[j]-vel_space[indice1]) * coef

  return mask

def Chisq(spec, wave, sigma, vel_space, cwave, norm_coef, Z):
  """
  faster way to calc chisq for one line
  possible candidate for Cython optimization
  
  input:
    cwave: scalar [float]
    wave: numpy 1d array [float]
    vel_space: numpy 1d array [float]
    norm_coef: scalar [float], optional
    spec: numpy 1d array [float]
    sigma: numpy 1d array [float]
    Z; numpy 1d array [float]
  
  return:
    chisq_val: scalar [float]
  """
  
  vstep = ((vel_space[1] - vel_space[0]) + (vel_space[-1] - vel_space[-2])) / 2.0
  coef = norm_coef / vstep

  vel_space_line = c * (wave - cwave) / cwave

  m = vel_space.size
  #old slow code
  #new_spec = np.zeros(n, dtype = np.float64)
  #for j in range(m):
  #    cond1 = vel_space <= vel_space_line[j]
  #    indice1 = np.where(cond1)[0][-1]
  #    indice2 = indice1 + 1
  #    new_spec[j] += (vel_space[indice2]-vel_space_line[j]) * coef * Z[indice1]
  #    new_spec[j] += (vel_space_line[j]-vel_space[indice1]) * coef * Z[indice2]

  code = """
  int indice1, indice2;
  double result, tmp_val;
  double ncoef;
  ncoef = coef;

  result = 0.0;
  for (int j=0; j<m; j++) {
    indice2 = 0;
    while((indice2 < m-1) & (vel_space(indice2) <= vel_space_line(j)))
      indice2++;
    indice1 = indice2 - 1;
    tmp_val = (vel_space(indice2) - vel_space_line(j)) * ncoef * Z(indice1);
    tmp_val += (vel_space_line(j) - vel_space(indice1)) * ncoef * Z(indice2);
    result += ((spec(j) - tmp_val) * (spec(j) - tmp_val)) / (sigma(j) * sigma(j));}
  
  return_val = result; """

  chisq_val = weave.inline(code, ['m', 'vel_space', 'vel_space_line', 'coef', 'Z', 'spec', 'sigma'], 
                        type_converters=converters.blitz, compiler = 'gcc')
  return chisq_val

def reduceChisq(chisq, sigZ, dof):
  """
  reduces chisq for dof and the fixes sigZ
  input:
    chisq: scalar [float]
    sigZ: numpy 1d array [float]
    dof: scalar [int]
   
  return:
    reduced_chisq: scalar [float]
  """
  
  reduced_chisq = chisq / dof
  coef = np.max([1.0,np.sqrt(reduced_chisq)])
  sigZ *= coef
  return reduced_chisq

def UpdateSystem(A, b, fspec, fmask, fweights):
  """
  Updates A and b.
  candidate for Cython optimization
  
  input:
    A: numpy 2d array [float]
    b: numpy 1d array [float]
    fspec: numpy 1d array [float]
    fmask: numpy 2d array [float]
    fweights: numpy 1d array [float]
  
  updates:
    A: numpy 2d array [float]
    b: numpy 1d array [float]
  """
  
  n, m = fmask.shape
  #original NumPy version (very slow)
  #fmaskT = fmask.transpose()
  #tmpSY = fspec * fweights
  #for i in range(m):
  #  for j in range(m):
  #    A[i][j] += np.sum(fmaskT[i] * fmaskT[j] * fweights)
  #  b[i] += np.dot(fmaskT[i], tmpSY)
  
  code = """
  for (int i=0; i<m; i++)
    for (int j=0; j<m; j++)
      for (int k=0; k<n; k++)
        A(i,j) += fmask(k,i) * fmask(k,j) * fweights(k);
  
  for (int i=0; i<m; i++)
    for (int k=0; k<n; k++)
      b(i) += fmask(k,i) * fspec(k) * fweights(k);
  """
  
  weave.inline(code, ['n', 'm', 'A', 'b', 'fmask', 'fweights', 'fspec'],
               type_converters=converters.blitz, compiler='gcc')

def LSDSpectrum(A, b):
  """
  Given A and b calculates Z with LU decomposition and
  sigZ through the inverse matrix of A
  
  input:
    A: numpy 2d array [float]
    b: numpy 1d array [float]
  
  return:
    Z: numpy 1d array [float]
    sigZ: numpy 1d array [float]
  """
  lu, piv = linalg.lu_factor(A, overwrite_a=0)
  Z = linalg.lu_solve((lu, piv), b, trans=0, overwrite_b=0)

  #this is done to calculate the sigma of b
  invA = linalg.inv(A)
  sigZ = np.sqrt(invA.diagonal())
  return Z, sigZ
  #Z is the main result, in sigZ we have the variances

def Rfunc(Z):
  """
  this function if not needed!
  """
  return np.sum((Z[1:-1] - Z[0:-2])**2.0) + np.sum((Z[1:-1] - Z[2:])**2.0)

def MeanField(I, V, line0, geff0, vel_space):
  """
  line0 in angstrems
  vel_space in km/s
  works on top of everything
  
  input:
    I: numpy 1d array [float]
    V: numpy 1d array [float]
    line0: scalar [float]
    geff0: scalar [float]
    vel_space: numpy 1d array [float]
  
  return:
    Be: scalar [float]
  """
  coef = -7.145e6 / (line0 * geff0)
  funcV = interpolate.splrep(vel_space, V * vel_space, s = 0.0)
  integralV = interpolate.splint(vel_space[0], vel_space[-1], funcV)
  funcI = interpolate.splrep(vel_space, I, s=0.0)
  integralI = interpolate.splint(vel_space[0], vel_space[-1], funcI)

  Be = (integralV / integralI) * coef
  return Be

class Regularization:
  def __init__(self, indexes, spec, sig, wave, vel_space, cwave, norm_vector, A, b, Z, sigZ, matrixR):
    self.indexes = indexes
    self.spec = spec
    self.sig = sig
    self.wave = wave
    self.vel_space = vel_space
    self.cwave = cwave
    self.norm_vector = norm_vector
    self.A = A
    self.b = b
    self.Z = Z
    self.sigZ = sigZ
    self.matrixR = matrixR

  def __call__(self, Lambda):
    #calculate the regularized matrix Areg
    Areg = self.A + Lambda*self.matrixR

    #calculate the new Z
    self.Z, self.sigZ = LSDSpectrum(Areg, self.b)

    #calculate chisq
    chisq = 0.0
    #print('Chisq:')
    for i in range(len(self.indexes)):
      #print('line: %3d, %9.3f' %(i, self.cwave[i]))
      index_start = self.indexes[i][0]
      index_end = self.indexes[i][1]+1
      fspec = self.spec[index_start:index_end]
      fsigma = self.sig[index_start:index_end]
      fwave = self.wave[index_start:index_end]
      chisq += Chisq(fspec, fwave, fsigma, self.vel_space, self.cwave[i], self.norm_vector[i], self.Z)
    #Rf = Rfunc(self.Z)
    Rf = np.sum((self.Z[1:-1] - self.Z[0:-2])**2.0) + np.sum((self.Z[1:-1] - self.Z[2:])**2.0)
    return chisq + Rf * Lambda**2.0 / 2.0

class ZeemanLSD:
  #NOTE: This current version works fine for theoretical 1d spectra, but
  #NOTE: for real ech spectra doesn't work very well.
  #NOTE: Instead changing the entire code I wrote additional code in the preparatory part, 
  #NOTE: where we choose sp.lines to make it conform with the way we calculate everything here,
  def __init__(self, wave, spec, sig, indexes, cwave, depth, geff, vel_space, ZeemanType):
    """
    loader method
    """
    self.wave = np.array(wave, copy=True, dtype=np.float64)
    self.spec = np.array(spec, copy=True, dtype=np.float64)
    self.sig =  np.array(sig, copy=True, dtype=np.float64)
    self.indexes = np.array(indexes, copy=True, dtype=np.int32)
    self.cwave = np.array(cwave, copy=True, dtype=np.float64)
    self.depth = np.array(depth, copy=True, dtype=np.float64)
    self.geff = np.array(geff, copy=True, dtype=np.float64)
    self.vel_space = np.array(vel_space, copy=True, dtype=np.float64)
    self.ZeemanType = ZeemanType
    self.cwave0 = None
    self.depth0 = None
    self.geff0 = None
    self.norm_coef = None
    self.norm_vector = None
    self.A = None
    self.b = None
    self.Z = None
    self.sigZ = None
    self.matrixR = None
    self.Rval = None
    self.chisq = None
    self.reducedchisq = None
    self.Lambda = None

  def Prepare(self, Lambda = None):
    """
    calculates the normalization coefficients, checks the arrays from the __init__ method and
    if everything passes creates the mask matrix.
    NOTE: for the calculation of the normalization coefficient(s) self.ZeemanType is used
    """

    self.cwave0, self.geff0, self.depth0 = NormalizationCoefficients(self.indexes, self.sig, self.cwave, self.geff, self.depth)

    if self.ZeemanType.upper() == 'I':
      self.norm_coef = self.depth0
    elif self.ZeemanType.upper() == 'V':
      self.norm_coef = self.cwave0 * self.depth0 * self.geff0
    else:
      raise ValueError("ZeemanType should be one of: 'I', 'V', 'U', 'Q'")

    #Now checking if all arrays have the required shape and size
    if len(self.wave.shape) != 1:
      raise IndexError("wave should have shape 1")
    if len(self.spec.shape) != 1:
      raise IndexError("spec should have shape 1")
    if len(self.sig.shape) != 1:
      raise IndexError("spec should have shape 1")

    if len(self.indexes.shape) != 2:
      raise IndexError("indexes should be 2d numpy array")

    n = 0
    for i in range(self.indexes.shape[0]):
      n_tmp = self.indexes[i][1] - self.indexes[i][0]
      if n_tmp <= 0:
        raise ValueError("Something wrong with row %3d in indexes." %i)
      else:
        n += n_tmp + 1

    #if self.wave.size != n:
    #  raise IndexError("Size of wave does not conform with the size derived from indexes")
    #if self.spec.size != n:
    #  raise IndexError("Size of spec does not conform with the size derived from indexes")
    #if self.sig.size != n:
    #  raise IndexError("Size of sig does not conform with the size derived from indexes")

    num_lines = self.indexes.shape[0]
    if self.cwave.size != num_lines:
      raise IndexError("Size of cwave does not conform with the size derived from indexes")
    if self.geff.size != num_lines:
      raise IndexError("Size of geff does not conform with the size derived from indexes")
    if self.depth.size != num_lines:
      raise IndexError("Size of depth does not conform with the size derived from indexes")

    #once everything has been checked we can load the norm_vector
    if self.ZeemanType.upper() == 'I':
      self.norm_vector = (self.depth / self.norm_coef)
    elif self.ZeemanType.upper() == 'V':
      self.norm_vector = (self.cwave * self.depth * self.geff) / self.norm_coef
    else:
      raise ValueError("ZeemanType should be one of: 'I', 'V', 'U', 'Q'")

    #end of prepare

  def BuildSystem(self):
    """
    candidate for Cython optimization
    """
    #First fix the matrix A and the vector b just in case is we've alredy ran this method
    self.A = np.zeros((self.vel_space.size, self.vel_space.size), dtype=np.float64)
    self.b = np.zeros(self.vel_space.size, dtype = np.float64)

    #now load A nd b
    for i in range(len(self.indexes)):
      print('line: %3d, %9.3f' %(i, self.cwave[i]))
      index_start = self.indexes[i][0]
      index_end = self.indexes[i][1]+1
      fspec = self.spec[index_start:index_end]
      fweights = 1.0 / self.sig[index_start:index_end]**2.0
      fwave = self.wave[index_start:index_end]
      fmask = MaskCreator(self.cwave[i], fwave, self.vel_space, self.norm_vector[i])
      UpdateSystem(self.A, self.b, fspec, fmask, fweights)

  def ZeemanProfileNoRegularization(self, BuildSystem=False):
    """
    After all the preparations calculate the Zeeman profiles.
    If BuildSystem is True run BuildSystem first.
    """

    #sanity checks
    if self.A is None or self.b is None:
      self.BuildSystem()
    elif BuildSystem: #on user request
      self.BuildSystem()

    print('Start LSD: No Regularization. ZeemanProfile: '+self.ZeemanType)
    self.Z, self.sigZ = LSDSpectrum(self.A, self.b)

    #calculate chisq
    self.chisq = 0.0
    print('Chisq:')
    for i in range(len(self.indexes)):
      print('line: %3d, %9.3f' %(i, self.cwave[i]))
      index_start = self.indexes[i][0]
      index_end = self.indexes[i][1]+1
      fspec = self.spec[index_start:index_end]
      fsigma = self.sig[index_start:index_end]
      #fweights = 1.0 / fsigma**2.0
      fwave = self.wave[index_start:index_end]
      self.chisq += Chisq(fspec, fwave, fsigma, self.vel_space, self.cwave[i], self.norm_vector[i], self.Z)
    print('chisq = ',self.chisq)

    #number of data points
    n = 0
    for i in range(self.indexes.shape[0]):
      n += self.indexes[i][1] - self.indexes[i][0] + 1

    self.reducedchisq = reduceChisq(self.chisq, self.sigZ, dof=(n - self.vel_space.size))
    print('chisq/dof = ', self.reducedchisq)
    print('End LSD: No Regularization. ZeemanProfile: '+self.ZeemanType)

  def ZeemanProfileRegularization(self, Lambda=None, BuildSystem=False):
    """
    Employs Tikhonov regularization
    if Lambda is None:
      Lambda = trace(A) / trace(matrixR)
    NOTE: trace(matrixR) = 2 * vel_space.size
    if BuildSystem is True first will run BuildSystem,
    and then it will calculate normalized profile
    """

    #sanity checks
    if self.A is None or self.b is None:
      self.BuildSystem()
    elif BuildSystem: #on user request
      self.BuildSystem()

    if self.matrixR is None:
      self.matrixR = np.zeros((self.vel_space.size, self.vel_space.size), dtype=np.float64)
      load_matrixR(self.matrixR)

    print('Start LSD: Regularization. ZeemanProfile: '+self.ZeemanType)
    RegFunc = Regularization(self.indexes, self.spec, self.sig, self.wave,
                             self.vel_space, self.cwave, self.norm_vector,
                             self.A, self.b, self.Z, self.sigZ, self.matrixR)

    if Lambda is None:
      Lambda = self.A.trace() / np.float64(2.0 * self.vel_space.size)
    print('Lambda: '+str(Lambda))

    RegFunc(Lambda)
    self.Lambda = Lambda
    self.Z = RegFunc.Z
    self.sigZ = RegFunc.sigZ

    self.Rval = np.sum((self.Z[1:-1] - self.Z[0:-2])**2.0) + np.sum((self.Z[1:-1] - self.Z[2:])**2.0)

    #calculate chisq
    self.chisq = 0.0
    print('Chisq:')
    for i in range(len(self.indexes)):
      print('line: %3d, %9.3f' %(i, self.cwave[i]))
      index_start = self.indexes[i][0]
      index_end = self.indexes[i][1]+1
      fspec = self.spec[index_start:index_end]
      fsigma = self.sig[index_start:index_end]
      fwave = self.wave[index_start:index_end]
      self.chisq += Chisq(fspec, fwave, fsigma, self.vel_space, self.cwave[i], self.norm_vector[i], self.Z)
    print('chisq = ',self.chisq)

    #number of data points
    n = 0
    for i in range(self.indexes.shape[0]):
      n += self.indexes[i][1] - self.indexes[i][0] + 1

    self.reducedchisq = reduceChisq(self.chisq, self.sigZ, dof=(n - self.vel_space.size))
    print('chisq/dof = ', self.reducedchisq)
    print('End LSD: Regularization. ZeemanProfile: '+str(self.ZeemanType))

  def PlotZeemanProfile(self, fname=None):
    fig = plt.figure()
    ax = fig.add_subplot(111)

    if self.ZeemanType.upper() == 'I':
      plot_vector = 1.0 - self.Z
      color = 'red'
      title = 'LSD Stokes I profile'
      if fname is None:
        fname = 'LSDStokesI'
    elif self.ZeemanType.upper() == 'V':
      plot_vector = self.Z
      color = 'blue'
      title = 'LSD Stokes V profile'
      if fname is None:
        fname = 'LSDStokesV'
    else:
      raise ValueError("ZeemanType should be one of: 'I', 'V', 'U', 'Q'")

    ax.plot(self.vel_space, plot_vector, color=color, lw=1.0)
    ax.grid()
    ax.set_xlim(self.vel_space[0],self.vel_space[-1])
    ax.set_xlabel('Velocity, km/s')
    ax.set_title(title)
    fig.show()
    fig.savefig(fname)
    return fig

  def SystemToFile(self, fname=None):
    if fname is None:
      fname = 'LSD_data_'+str(self.ZeemanType)

    np.savez(fname, self.A, self.b)
    print('Written to: '+fname)

  def SystemFromFile(self, fname=None):
    if fname is None:
      fname = 'LSD_data_'+str(self.ZeemanType)+'.npz'

    res = np.load(fname)
    self.A = res['arr_0']
    self.b = res['arr_1']
    print('Read from: '+fname)
