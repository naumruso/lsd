from __future__ import division, print_function, absolute_import

#import pdb
import numpy as np

from scipy.constants import c as vc
from collections import Counter

from _fmodules import fspeeders
from _pylsdf import lsd_core


# speed of light in km/s
vc /= 1000.0

# maximum number of species
maxspecies = 3

# field names for the lines data
_wlc = 'wlc'
_depth = 'depth'
_el = 'el'
_gl = 'gl'
_gmean = 'gmean'
_loggf = 'loggf'
_ei = 'ei'

# field names for the observations
_wl = 'wl'
_sp = 'sp'
_sig_sp = 'sigsp'

# field names for the lsd profiles
_vel = 'vel'
_Z = 'Z'
_sigZ = 'sigZ'


def flatten(l, ltypes=(list, tuple)):
    """
    Taken from http://basicproperty.sourceforge.net/
    """
    ltype = type(l)
    l = list(l)
    i = 0
    while i < len(l):
        while isinstance(l[i], ltypes):
            if not l[i]:
                l.pop(i)
                i -= 1
                break
            else:
                l[i:i + 1] = l[i]
        i += 1
    return ltype(l)


def stand_mad(x, c=0.67448975019608171, axis=None):
    """
    The standardized Median Absolute Deviation along given axis of an array.

    Parameters
    ----------
    x : array-like
        Input array.
    c : float, optional
        The normalization constant.  Defined as scipy.stats.norm.ppf(3/4.),
        which is approximately .6745.
    axis : int, optional
        The defaul is 0 (None).

    Note: I pretty much copied the code from statsmodels library.
    """
    return np.median(np.abs(x - np.median(x, axis=axis)), axis=axis) / c


# create the empty class for storing results
class Container(object):
    pass


def average_SN(siglsd, vsiz, nspecies):
    """
    Computes average S/N ratio for each LSD profile
    """
    SN = []
    for i in range(nspecies):
        SN += [np.int32(vsiz / np.sum(siglsd[i*vsiz:(i+1)*vsiz]))]
    return SN


def compute_mcommon(lines, fields, msig, elem=None, idx_species=None):
    try:
        lines[_el]
    except:
        raise AttributeError("The lines array does not have field %s \
                             in it." % _el)

    for field in fields:
        try:
            lines[field]
        except:
            raise AttributeError("The lines array does not have field %s \
                                 in it." % field)

    if idx_species is not None:
        if elem is None:
            raise ValueError("If idx_species is present, elem also has to \
                             be present.")

    # select the species that we want
    if idx_species is None:
        idx = np.arange(lines.size)
    else:
        idx, = np.where(elem == idx_species)

    # work with selected species
    c = Counter(lines[_el][idx])
    (el, nel) = c.most_common()[0]
    idx, = np.where(lines[_el] == el)
    mcommon_ion = {}
    mcommon_ion['el'] = el
    mcommon_ion['nel'] = nel
    smsig = np.sum(msig[idx])
    for field in fields:
        mcommon_ion[field] = np.sum(lines[field][idx] * msig[idx]) / smsig
    return mcommon_ion


def run_lsd(obs, lines, **kwargs):
    """
    lines: numpy record array
           required fields:
             wlc - central wavelenths
             depth - line strengths
             gmean - mean Lande factors
             el - element name
             loggf - loggf for line transition
             gl - alternative linear polariz. weights, used if qualt is True.

    obs: numpy record array
           required fields:
             wl - wavelengths
             sp - observed spectrum
             sigsp - errors for observed spectrum

    In lines and obs all fields are supposed to be 1d arrays.

    Optional parameters:
      sp_type:  string, default='i'; possible values: 'i', 'v', 'q', 'u'
      verbose: boolean, default=True
      cutoff: float, default=0.1
      vstart: float, default=-80.0
      vend: float, default=80.0
      vstep: float, default=0.8
      qualt: boolean, default=False
      depth_adjust: float, default=3.0
      sigmaclip: integer, default=10
      cliplimit: integer, default=5
      robust: boolean, default=False
      gaps: None or iterable, default=None
      dl: float, default=1.0
      wl0: float, default=1.0
      depth0: float, default=1.0
      lande0: float, default=1.0
      species: None or list, default=None
      fields: list of strings, default = [_wlc, _depth, _gmean, _loggf, _ei]
      reg: float, default=0.0
      lscl: int, default=1
      verif: int, default=1

    Comments:
      robust: If set to True pylsd uses standard MAD for the sigma clipping
          procedure.

      reg: default is no regularization. If set to a value less than 0 zero
          then pylsd uses default value. If reg is set to a value greater
          than zero then then pylsd uses the given value.
    """
    # parsing input parameters
    sp_type = kwargs.setdefault('sp_type', 'i')
    verbose = kwargs.setdefault('verbose', True)
    cutoff = kwargs.setdefault('cutoff', 0.1)
    vstart = kwargs.setdefault('vstart', -80.0)
    vend = kwargs.setdefault('vend', 80.0)
    vstep = kwargs.setdefault('vstep', 0.8)
    qualt = kwargs.setdefault('qualt', False)
    depth_adjust = kwargs.setdefault('depth_adjust', 3.0)
    sigmaclip = kwargs.setdefault('sigmaclip', 10)
    cliplimit = kwargs.setdefault('cliplimit', 5)
    robust = kwargs.setdefault('robust', False)
    gaps = kwargs.setdefault('gaps', None)
    dl = kwargs.setdefault('dl', 1.0)
    wl0 = kwargs.setdefault('wl0', 1.0)
    depth0 = kwargs.setdefault('depth0', 1.0)
    lande0 = kwargs.setdefault('lande0', 1.0)
    species = kwargs.setdefault('species', None)
    fields = kwargs.setdefault('fields', [_wlc, _depth, _gmean, _loggf, _ei])
    reg = kwargs.setdefault('reg', 0.0)
    lscl = kwargs.setdefault('lscl', 1)
    verif = kwargs.setdefault('verif', 1)

    # set the polarization flag
    if sp_type.lower() == 'i':
        pol = 0
    else:
        pol = 1
    kwargs['pol'] = pol

    # original number of sp. lines
    nlin_start = lines.size
    if verbose:
        print('Lines at start: %i' % nlin_start)

    # select lines by the cutoff and negative Lande criteria
    idx, = np.where(fspeeders.cutofflande(lines[_depth], lines[_gmean], cutoff,
                    0.0) >= 0)
    lines = lines[idx]
    nlin_cutofflande = lines.size
    if verbose:
        print('Lines after cutoff and positive lande: %i' % nlin_cutofflande)

    # remove lines in gaps, widen each gap by amount dl from each side
    if gaps is not None:
        gaps1dlin = np.array(gaps, dtype=np.float64).flatten()
        gaps1dlin[0::2] -= dl
        gaps1dlin[1::2] += dl
        idx, = np.where(fspeeders.locate_gaps(lines[_wlc], gaps1dlin) >= 0)
        lines = lines[idx]
        nlin_gaps = lines.size
        if verbose:
            print('Lines after removal of gaps: %i' % nlin_gaps)

    # original number of pixels
    npix_start = obs[_wl].size
    if verbose:
        print('Pixels at start: %i' % npix_start)

    # remove pixels in gaps
    if gaps is not None:
        gaps1dpix = np.array(gaps, dtype=np.float64).flatten()
        idx, = np.where(fspeeders.locate_gaps(obs[_wl], gaps1dpix) >= 0)
        obs = obs[idx]
        npix_gaps = obs[_wl].size
        if verbose:
            print('Pixels after removal of gaps: %i' % npix_gaps)

    # check for NaN values in the observed spectrum
    cond = np.isfinite(obs[_sp]) & np.isfinite(obs[_sig_sp])
    idx, = np.where(cond)
    if idx.size > 0:
        obs = obs[idx]
        npix_naninf = obs.size
        if verbose:
            print("Pixels left after removal of NaN and Inf points: %i" %
                  npix_naninf)

    # we have list with different species
    # species = [species1, species2, species3, ...., speciesN]
    # species1,..., speciesN are all lists of element names
    # "element" here means, e.g., Fe, Fe 1, ..., Ca 2, etc., one string
    #pdb.set_trace()
    if species is None:
        nspecies = 1
    else:
        nspecies = len(species)

        if nspecies > maxspecies:
            raise ValueError("No more than 3 species")

        # for multiple species we need to have the field _el in lines
        try:
            lines[_el]
        except:
            raise ValueError('For multiple species the field el in lines \
                             record array is required.')

        # preparing the array with the elements and their ionisation state
        # they are in the form 'ELEMENT IONSTATE'
        elem = []
        for el in lines[_el]:
            elem.append((el.split()[0].lower(), int(el.split()[1].lower())))

        # numbering the species and parsing the ionisation states
        species_tmp = []
        for i in range(len(species)):
            for j in range(len(species[i])):
                #check if there is any ionisation information
                #if there is species[i][j] should be a string
                #in the form 'ELEMENT IONSTATE_1 IONSTATE_2... IONSTATE_N'
                c_elem = species[i][j].split()[0].lower()
                n_ions = len(species[i][j].split()) - 1

                if n_ions ==  0: #nothing to declare
                    species_tmp.append((c_elem, None, i))
                elif n_ions >= 1: #only for selected ionisation states
                    c_ions = np.asarray(species[i][j].split()[1:], dtype=np.int32) #currently selected ionisation states
                    for c_ion in c_ions:
                        species_tmp.append((c_elem, c_ion, i))
                else:
                    raise ValueError("Appears that species contains a malformed string. species[i][j]=%s" %species[i][j])
        species = species_tmp

        # finding what element belongs to what species
        # numbering of species is from zero
        for i in range(len(elem)):
            for j in range(len(species)):
                if elem[i][0] == species[j][0]: #we have match for the element name
                    #now check the ionisation state
                    if species[j][1] is None or species[j][1] == elem[i][1]:
                        elem[i] = species[j][2]
                        break

        # now remove elements that don't belong to any species
        for i in range(len(elem)):
            if not isinstance(elem[i], int):
                elem[i] = None
        idx, = np.where(np.not_equal(elem, None))
        lines = lines[idx]
        elem = np.array(elem)[idx]
        if verbose:
            print('Spectral lines that belong to selected species: %i' %
                  len(elem))

    # report preliminary means
    pmean_wlc = lines[_wlc].mean()
    pmean_depth = lines[_depth].mean()
    pmean_gmean = lines[_gmean].mean()
    if verbose:
        print("Preliminary mean line parameters: pmean_wl0=%8.3f, \
              pmean_d0=%5.3f, pmean_g0=%5.3f" %
              (pmean_wlc, pmean_depth, pmean_gmean))

    # adjust line depths
    nlin_adjust = 0
    if depth_adjust > 0:
        depth_tmp = np.array(lines[_depth], copy=True, dtype=np.float64)
        for i in range(lines.size):
            idx, = np.where(np.fabs(lines[_wlc] - lines[_wlc][i]) /
                            lines[_wlc][i]*vc <= depth_adjust)
            if np.sum(lines[_depth][idx]) > 1.0:
                depth_tmp[i] = depth_tmp[i]/np.sum(lines[_depth][idx])
                nlin_adjust += 1
        if verbose:
            print('Line depths adjusted: %d' % nlin_adjust)
        lines[_depth] = depth_tmp

    # calculate weights
    if sp_type.lower() == 'i':
        weight = (lines[_depth] / depth0)
    elif sp_type.lower() == 'v':
        weight = (lines[_gmean] / lande0) * \
                 (lines[_wlc] / wl0) * \
                 (lines[_depth] / depth0)
    elif sp_type.lower() == 'q' or sp_type.lower() == 'u':
        weight = (lines[_wlc] / wl0)**2 * (lines[_depth] / depth0)
        if qualt:  # alternative weights
            weight = weight * lines[_gl]
        else:
            weight = weight * lines[_gmean]**2

    # compute the velocity space
    # this is done in the same manner as in lsd4
    if vend < vstart:
        vstart, vend = (vend, vstart)
    vsiz = int(round((vend - vstart)/vstep)) + 1
    vlsd = vstart + np.arange(vsiz) * vstep
    vend = vlsd[-1]

    # substract the observed spectrum from unity
    # if we're dealing with intensity spectrum
    if sp_type.lower() == 'i':
        obs[_sp] = 1.0 - obs[_sp]

    # now create the matrix with the line weights
    # dim(WGTLIN) = (LSDSIZ, LINSIZ)
    wgtlin = np.zeros((nspecies, weight.size), order='F', dtype=np.float64)
    if nspecies == 1:
        wgtlin[0, :] = weight
    else:
        for i in range(weight.size):
            wgtlin[elem[i], i] = weight[i]

    # run lsd_core.preinit_lsd on our data. it will remove sp. points not
    # covered by any line and more the rest of the points to the beginningm
    # of the arrays. Due to the previous operations on obs, the array is
    # no longer contigious in memory. I can not make it with one command
    # obs = np.asfortranarray(obs) therefore the following hack that works.
    # Note: all arrays are previously copied if they are not Fortran contiguous
    # in memory. In the case of intent(in) arrays this is done in an
    # automated way by f2py.
    obs_wl = np.asfortranarray(obs[_wl])
    obs_sp = np.asfortranarray(obs[_sp])

    # from here on _sig_sp is actually weights.
    obs_sig_sp = np.asfortranarray(1./obs[_sig_sp])

    nwl, ierr = lsd_core.preinit_lsd(lines[_wlc], vlsd[0], vlsd[-1],
                                     obs_wl, obs_sp, obs_sig_sp)
    if ierr != 0:
        raise StandardError('Error reported: _pylsdf.lsd_core.preinit_lsd, \
                            IERR=%d' % ierr)
    obs[_wl] = obs_wl
    obs[_sp] = obs_sp
    obs[_sig_sp] = obs_sig_sp
    if verbose:
        print('Number of spectral points contributing to any spectral line: \
              nwl=%s' % nwl)

    # start of the sigma-clipping procedure
    iterat = 1
    while True:
        # init line-pattern matrix, weighted mean line weights and lines used
        mm, mwgt, msig, nlinuse, ierr = \
            lsd_core.init_lsd(nwl, obs[_wl], obs[_sig_sp],
            lines[_wlc], wgtlin, vlsd[0], vlsd[-1], vstep, vlsd)
        if ierr != 0:
            raise StandardError('Error reported: _pylsdf.lsd_core.init_lsd, \
                                IERR=%d' % ierr)
        if verbose:
            print("Line-pattern matrix built.")

        # compute the lsd profile
        msp, plsd, siglsd, chisq_nu, msd, maxdev, da2, da1, ierr = \
            lsd_core.invert_lsd(nspecies, vlsd.size, mm, obs[_sp],
            obs[_sig_sp], reg, verif, lscl)
        if ierr != 0:
            raise StandardError('Error reported: _pylsdf.lsd_core.invert_lsd, \
                                IERR=%d' % ierr)
        if verbose:
            print("LSD profiles computed.")
        sqrt_chisq_nu = np.sqrt(chisq_nu)
        SN = average_SN(siglsd, vlsd.size, nspecies)

        if verbose:
            print('nspecies:%s, nlinuse:%s, mweight:%s' %
                  (nspecies, nlinuse, mwgt))
            print('msd:%s, maxdev:%s, chisq/nu: %s' % (msd, maxdev, chisq_nu))
            print('SN:%s' % SN)

        # clipping any pixel that goes beyond the sigmaclip value
        if sigmaclip > 0 and sp_type.lower() != 'i':
            dif = obs[_sp][:nwl] - msp[:nwl]
            if robust:
                stddev = stand_mad(dif)
            else:
                stddev = np.std(dif, ddof=1)

            idx, = np.where(np.fabs(dif) < sigmaclip * stddev)
            nbad = nwl - idx.size
            if nbad > cliplimit:
                obs = obs[idx]
                nwl -= nbad
                del idx
            else:
                break

            if verbose:
                print('sigmaclip=%i, pixels removed:%i, iteration:%i' %
                      (sigmaclip, nbad, iterat))
                print('nwl=%i' % nwl)
        else:
            break
        iterat += 1

    # reshaping the results according to nspecies
    # this is mostly done so that the results from
    # this version is compatible with the previous one
    plsd = plsd.reshape((nspecies, vlsd.size))
    siglsd = siglsd.reshape((nspecies, vlsd.size))
    lsd = []
    for profile, sig_profile in zip(plsd, siglsd):
        prof = np.array(zip(vlsd, profile, sig_profile),
                        dtype=[(_vel, np.float64),
                               (_Z, np.float64),
                               (_sigZ, np.float64)])
        lsd.append(prof.view(np.recarray))

    # normalization of the final line weights.
    # these weights are constructed according to
    # how many times the line has been used the
    # construction of the line-pattern matrix
    msig = msig / np.sum(msig)

    # final averages after taking into account observational
    # errors and possible line rejections
    if verbose:
        print('Final total number of lines:%i' % msig.size)
    wfinal_wlc = np.sum(lines[_wlc] * msig)
    wfinal_depth = np.sum(lines[_depth] * msig)
    if qualt:
        wfinal_lande = np.sum(lines[_gl] * msig)
    else:
        wfinal_lande = np.sum(lines[_gmean] * msig)
    if verbose:
        print("Final weighted mean line parameters: wfinal_wlc=%8.3f, \
              wfinal_lande=%5.3f, wfinal_depth=%5.3f" %
              (wfinal_wlc, wfinal_lande, wfinal_depth))

    # parameters of mean line for the most common ion for each species.
    # Also works when we don't have more than one species.
    mcommon_species = []
    if species is not None:
        for i in range(nspecies):
            mcommon_ion = compute_mcommon(lines, fields, msig, elem, i)
            mcommon_species.append(mcommon_ion)
            if verbose:
                print('Most common ion for species %s: %s, \
                      Nlines:%i, cwave_c:%8.3f, depth_c: %5.3f, \
                      lande_c: %5.3f, loggf_c: %7.3f, ei_c: %6.3f' %
                      (i, mcommon_ion['el'], mcommon_ion['nel'],
                          mcommon_ion[_wlc], mcommon_ion[_depth],
                          mcommon_ion[_gmean], mcommon_ion[_loggf],
                          mcommon_ion[_ei]))

    # the same thing for the entire mask (This is always last in the list)
    # Also this is not always possible if we don't have el field in lines.
    mcommon_all = compute_mcommon(lines, fields, msig)
    if verbose:
        print('Most common ion:%s, Nlines:%i, cwave_c:%8.3f, depth_c: %5.3f, \
              lande_c:%5.3f, loggf_c:%7.3f, ei_c:%6.3f' %
              (mcommon_all['el'], mcommon_all['nel'], mcommon_all[_wlc],
               mcommon_all[_depth], mcommon_all[_gmean], mcommon_all[_loggf],
               mcommon_all[_ei]))

    # saving everything
    results = Container()
    results.input_parameters = kwargs  # values of all program parameters

    results.nlin_start = nlin_start
    results.nlin_cutofflande = nlin_cutofflande
    results.nlin_adjust = nlin_adjust
    results.nlin_gaps = nlin_gaps

    results.npix_start = npix_start
    results.npix_gaps = npix_gaps
    results.npix_naninf = npix_naninf

    results.nspecies = nspecies

    results.pmean_wlc = pmean_wlc
    results.pmean_depth = pmean_depth
    results.pmean_gmean = pmean_gmean

    results.iterat = iterat

    results.species = species
    results.nspecies = nspecies

    results.mwgt = mwgt
    results.msd = msd
    results.da1 = da1
    results.da2 = da2
    results.maxdev = maxdev
    results.chisq_nu = chisq_nu
    results.sqrt_chisq_nu = sqrt_chisq_nu
    results.SN = SN
    results.nlinuse = nlinuse

    results.wfinal_wlc = wfinal_wlc
    results.wfinal_lande = wfinal_lande
    results.wfinal_depth = wfinal_depth

    results.lsd = lsd

    results.lines = lines
    obs[_sig_sp] = 1./obs[_sig_sp]
    results.obs = obs
    results.msig = msig

    results.mcommon = {'all': mcommon_all, 'species': mcommon_species}

    return results
