from __future__ import division, print_function, absolute_import

from os import path
package_dir = path.abspath(path.dirname(__file__))

__version__ = '1.0'

__all__ = ['pylsd']

try:
    from .pylsd import run_lsd, lsd_core, fspeeders
except:
    pass
