!------------------------------------------------------------------------------
! Uppsala University, Department of Physics and Astronomy,
! Least-Squares Deconvolution Code
!------------------------------------------------------------------------------
!
!> @author Rusomarov
!> @date April, 2013
!
!> @brief Implements all of the program error codes and messages.
!
!> @details Instead of writing and passing strings we pass integer
!> codes. All of the codes have their own text messages. The goal is to
!> make everything more portable by not using directly strings or numbers.
!> Everything in this module is visible. The integer names are parameters,
!> while the text messages are protected.
!> 
!> cerror_data.txt contains a table with two columns. The first column is
!> the first string, while everything else is the second column. In the
!> first column we have the names for the variables of the error codes,
!> while in the second one are the string messages. Empty string messages
!> are allowed to appear in the table, but in Fortran code you will still
!> have message with the code name.
!>
!> @note cerror.F90 has to be preprocessed with pyexpander.py to produce
!> Fortran code.
!>
!> Example of how cerror_data.txt should look like
!> - - -
!> cerror_error1 This is first error code. \n
!> cerror_error2 This is second error code. \n
!> ... \n
!> cerror_errorN This is N error code. \n
!> - - -
!
!> @todo Implement checks in pyexpander for uniqueness of the error names.
!> @todo Initialize the cerror messages automatically without calling
!> the subroutine cerror_init
!------------------------------------------------------------------------------
module cerror
  use cdata, only: i4b, iomsg_len
  
  implicit none





  
  integer(kind=i4b), parameter :: cerror_io_bad_input = 1
  
  integer(kind=i4b), parameter :: cerror_io_readline = 2
  
  integer(kind=i4b), parameter :: cerror_io_num_cols_amb = 3
  
  integer(kind=i4b), parameter :: cerror_io_eof = 4
  
  integer(kind=i4b), parameter :: cerror_io_filename = 5
  
  integer(kind=i4b), parameter :: cerror_io_openfile = 6
  
  integer(kind=i4b), parameter :: cerror_io_bad_obsdata = 7
  
  integer(kind=i4b), parameter :: cerror_io_bad_linelist = 8
  
  integer(kind=i4b), parameter :: cerror_io_format_velocity = 9
  
  integer(kind=i4b), parameter :: cerror_io_format_pol_flag = 10
  
  integer(kind=i4b), parameter :: cerror_alloc = 11
  
  integer(kind=i4b), parameter :: cerror_dealloc = 12
  
  integer(kind=i4b), parameter :: cerror_invert_matrix = 13
  

  integer(kind=i4b), parameter :: cerror_max_ncodes = 13 !< Max. number of error codes.
  character(len=iomsg_len) :: cerror_msg(cerror_max_ncodes) !< Contains the error messages.

  contains
    !---------------------------------------------------------------------------  
    !> @author NRusomarov
    !> @date April, 2013
    !
    !> @brief Initiates the names of the cerror vars. Done by the preprocessor.
    !
    !> @note After importing the module one has to call this subroutine to init
    !> the array that contains the error messages.
    !---------------------------------------------------------------------------  
    subroutine cerror_init()
      
      cerror_msg(cerror_io_bad_input) = "ERROR: bad input"
      
      cerror_msg(cerror_io_readline) = "ERROR: cannot read next line"
      
      cerror_msg(cerror_io_num_cols_amb) = "ERROR: ambiguous number of colums"
      
      cerror_msg(cerror_io_eof) = "ERROR: end-of-file reached"
      
      cerror_msg(cerror_io_filename) = "ERROR: cannot read filename from the config file"
      
      cerror_msg(cerror_io_openfile) = "ERROR: cannot open file for reading"
      
      cerror_msg(cerror_io_bad_obsdata) = "ERROR: observations data cannot have less than two columns"
      
      cerror_msg(cerror_io_bad_linelist) = "ERROR: line list data cannot have less than two columns"
      
      cerror_msg(cerror_io_format_velocity) = "ERROR: bad format in velocity limits and/or step"
      
      cerror_msg(cerror_io_format_pol_flag) = "ERROR: bad format in polarization flag"
      
      cerror_msg(cerror_alloc) = "ERROR: Array not allocated"
      
      cerror_msg(cerror_dealloc) = "ERROR: Array not deallocated"
      
      cerror_msg(cerror_invert_matrix) = "ERROR: Computation of the inverse of the autocorrelation matrix failed"
      
    end subroutine cerror_init

end module cerror
