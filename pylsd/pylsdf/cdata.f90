!------------------------------------------------------------------------------
! Uppsala University, Department of Physics and Astronomy, 
! Least-Squares Deconvolution Code
!------------------------------------------------------------------------------
!> @author NRusomarov
!> @date March, 2013
!
!> @brief Global constant data.
!
!> @details Module for keeping constant data and data blocks. Anything that
!> doesn't have to be visible to every program unit does not belong here.
!
!> @todo Write separate program under tests that checks the compiler kinds
!------------------------------------------------------------------------------
module cdata
  implicit none


  ! this is understood by f2py.
  ! note that in python we only have int and float, which
  ! are integer and double precision in Fortran.
  integer, parameter :: sp  = selected_real_kind(6, 37)    !single precision real (float)
  integer, parameter :: dp  = selected_real_kind(15, 307)  !double precision real (double)
  integer, parameter :: qp  = selected_real_kind(33, 4931) !quad precision real (long double)
  integer, parameter :: i1b = selected_int_kind(2)         !one byte integer (tiny)
  integer, parameter :: i2b = selected_int_kind(4)         !two bytes integer (short)
  integer, parameter :: i4b = selected_int_kind(8)         !four bytes integer (int)
  integer, parameter :: i8b = selected_int_kind(12)        !eight byte integer (long int)

  integer, parameter :: iomsg_len = 300 !< maximum length of string variables.
end module cdata
