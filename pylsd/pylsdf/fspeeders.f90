!f2py -c --fcompiler=gnu95 -m pyfspeeders fspeeders.f95
module fspeeders
use cdata
implicit none

contains
subroutine cutofflande(depth,gmean,dcutoff,gcutoff,n,m)
  implicit none
  integer(kind=I4B), intent(in) :: n
  real(kind=DP), intent(in) :: depth(n)
  real(kind=DP), intent(in) :: gmean(n)
  real(kind=DP), intent(in) :: dcutoff
  real(kind=DP), intent(in) :: gcutoff
  integer(kind=I4B), intent(out) :: m(n)

  !tmp. vars.
  integer :: i

  !load the array with the indices
  forall (i=1:n) m(i) = i - 1

  !find the bad lines
  where ((depth .lt. dcutoff) .or. (gmean .le. gcutoff)) m = -1
end subroutine cutofflande

!nrow = size(gaps,1)
!ncol = size(gaps,2)
subroutine locate_gaps(wl,nwl,gaps,ngaps,idx)
  implicit none
  integer(kind=I4B), intent(in) :: nwl
  real(kind=DP), intent(in) :: wl(nwl)
  integer(kind=I4B), intent(in) :: ngaps
  real(kind=DP), intent(in) :: gaps(ngaps)
  integer(kind=I4B), intent(out) :: idx(nwl)

  !temporary variables
  integer(kind=I4B) :: i,j

  !load the indices array
  forall (i=1:nwl) idx(i) = i - 1

  !find which pixels are located in gaps
  !-1 means pixel is in gap

  do i=1, nwl
    do j=1, ngaps, 2
      if ((wl(i) .ge. gaps(j)) .and. (wl(i) .le. gaps(j+1))) then
        idx(i) = -1
        exit
      endif
    enddo
  enddo
end subroutine locate_gaps
end module fspeeders
