"""
Unit test for the fortran lsd_core module.
"""
from __future__ import division, print_function, absolute_import

import os
import numpy as np

from numpy.testing import assert_, assert_equal,\
    assert_almost_equal, assert_array_almost_equal,\
    TestCase, run_module_suite

from pylsd import lsd_core, package_dir


class TestLsdCore(TestCase):
    """
    Test the three main fortran subroutines from pylsdf.lsd_core module.
    Compare the results for a simple numerical problem.
    """
    def setUp(self, decimal=5):
        data_dir = os.path.join(package_dir, 'tests/data/')

        lin_data = os.path.join(data_dir, 'test_core.lin.npz')  # line list
        obs_data = os.path.join(data_dir, 'test_core.obs.npz')  # observations
        wln_data = os.path.join(data_dir, 'test_core.wln.npz')  # line weights
        lsd_data = os.path.join(data_dir, 'test_core.lsd.npz')  # lsd profile
        mod_data = os.path.join(data_dir, 'test_core.mod.npz')  # calc. spectra

        self.lin = np.load(lin_data)
        self.obs = np.load(obs_data)
        self.wln = np.load(wln_data)
        self.lsd = np.load(lsd_data)
        self.mod = np.load(mod_data)

        self.vfirst = self.lsd['vlsd'][0]
        self.vlast = self.lsd['vlsd'][-1]
        self.vstep = self.lsd['vlsd'][1] - self.lsd['vlsd'][0]
        self.vsiz = self.lsd['vlsd'].size

        self.reg = np.float64(0.0)
        self.lscl = 1
        self.verif = 1
        self.nspecies = 1

        self.nwl = 19437
        self.nlinuse = 500
        self.mwgt = [3.4120e-01]
        self.da1 = 2.4425e-15
        self.da2 = 1.1369e-15
        self.msd = 4.9993e-02
        self.maxdev = 4.3725
        self.chisq_nu = 1.0137

        self.decimal = decimal

        # dirty trick, because I don't have precalculated mm
        # this should be fixed in the future
        self.mm, mwgt, msig, nlinuse, ierr = \
            lsd_core.init_lsd(self.nwl, self.mod['wl'], 1/self.mod['sigsp'],
            self.lin['wlclin'], self.lin['wgtlin'],
            self.vfirst, self.vlast, self.vstep, self.lsd['vlsd'])
        if ierr != 0:
            raise StandardError('Fortran subroutine _pyflsd.lsd_core.init_lsd \
                                returned IERR=%d' % ierr)

    def test_preinit_lsd(self):
        wl = np.array(self.obs['wl'], dtype=np.float64, copy=True)
        sp = np.array(self.obs['sp'], dtype=np.float64, copy=True)
        sigsp = np.array(self.obs['sigsp'], dtype=np.float64, copy=True)
        nwl, ierr = lsd_core.preinit_lsd(self.lin['wlclin'],
                                         self.vfirst, self.vlast,
                                         wl, sp, sigsp)

        assert_equal(nwl, self.nwl)
        assert_array_almost_equal(wl[:self.nwl], self.mod['wl'],
                                  decimal=self.decimal)
        assert_array_almost_equal(sp[:self.nwl], self.mod['sp'],
                                  decimal=self.decimal)
        assert_array_almost_equal(sigsp[:self.nwl], self.mod['sigsp'],
                                  decimal=self.decimal)

    def test_init_lsd(self):
        mm, mwgt, msig, nlinuse, ierr = \
            lsd_core.init_lsd(self.nwl, self.mod['wl'], 1/self.mod['sigsp'],
            self.lin['wlclin'], self.lin['wgtlin'], self.vfirst, self.vlast,
            self.vstep, self.lsd['vlsd'])
        assert_equal(nlinuse, self.nlinuse)
        assert_almost_equal(mwgt, self.mwgt, decimal=4)
        assert_array_almost_equal(msig, self.wln['msig'], decimal=self.decimal)

    def test_invert_lsd(self):
        msp, plsd, siglsd, chisq_nu, msd, maxdev, da2, da1, ierr = \
            lsd_core.invert_lsd(self.nspecies, self.vsiz, self.mm,
            self.mod['sp'], 1/self.mod['sigsp'],
            self.reg, self.verif, self.lscl)

        assert_almost_equal(chisq_nu, self.chisq_nu, decimal=4)
        assert_almost_equal(msd, self.msd, decimal=4)
        assert_almost_equal(maxdev, self.maxdev, decimal=3)
        assert_array_almost_equal(plsd, self.lsd['plsd'], decimal=self.decimal)
        assert_array_almost_equal(siglsd, self.lsd['siglsd'],
                                  decimal=self.decimal)
        assert_array_almost_equal(msp, self.mod['msp'], decimal=self.decimal)
        assert_almost_equal(da1, 0.0)
        assert_almost_equal(da2, 0.0)


if __name__ == '__main__':
    run_module_suite()
