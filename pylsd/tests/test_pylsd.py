"""
Unit test for the pylsd module.
"""
from __future__ import division, print_function, absolute_import

import os
import numpy as np
import cPickle as pickle

from numpy.testing import assert_, assert_equal,\
    assert_almost_equal, assert_array_almost_equal,\
    TestCase, run_module_suite

from pylsd import package_dir, run_lsd
from pylsd import pylsd


class TestPyLsd(TestCase):
    """
    Unit test for the pylsd module.
    """
    def setUp(self, decimal=5):
        data_dir = os.path.join(package_dir, 'tests/data/')
        data_file = 'obs.pyb'
        mask_file = 'mask.pyb'

        fin = open(os.path.join(data_dir, data_file), 'rb')
        self.obs = pickle.load(fin)
        fin.close()

        fin = open(os.path.join(data_dir, mask_file), 'rb')
        self.mask = pickle.load(fin)
        fin.close()

        self.cutoff = 0.1
        self.gaps = [[0.0, 3995.0],  # beginning, H lines up to Hepsilon
                     [4072.0, 4132.0],  # Hdelta
                     [4313.0, 4367.0],  # Hgamma
                     [4836.0, 4886.0],  # Hbeta
                     [5257.5, 5339.0],  # CCD gap
                     [5880.0, 6000.0],  # telluric
                     [6270.0, 6330.0],  # telluric
                     [6382.5, 6387.5],  # defect
                     [6460.0, 6590.0],  # telluric, Halpha (6562)
                     [6860.0, 9000.0]]  # telluric, end
        self.wl0 = 5000.0
        self.verbose = True
        self.species = None

    def test_run_lsd(self):
        # copy the input arrays, because the originals might get destroyed
        obs_in = np.array(zip(self.obs['wl'], self.obs['sx'], self.obs['gx']),
                          dtype=[(pylsd._wl, np.float64),
                                 (pylsd._sp, np.float64),
                                 (pylsd._sig_sp, np.float64)])
        mask_in = np.array(self.mask, copy=True)

        # loading all the subroutine params just in case they get changed
        kwargs = {}
        kwargs['sp_type'] = 'v'
        kwargs['verbose'] = self.verbose
        kwargs['cutoff'] = self.cutoff
        kwargs['vstart'] = -80.0
        kwargs['vend'] = 80.0
        kwargs['vstep'] = 0.8
        kwargs['qualt'] = False
        kwargs['depth'] = 3.0
        kwargs['sigmaclip'] = 10
        kwargs['cliplimit'] = 5
        kwargs['robust'] = False
        kwargs['gaps'] = self.gaps
        kwargs['dl'] = 1.0
        kwargs['wl0'] = self.wl0
        kwargs['depth0'] = 1.0
        kwargs['lande0'] = 1.0
        kwargs['species'] = self.species
        kwargs['reg'] = 0.0
        kwargs['lscl'] = 1
        kwargs['verif'] = 1

        # we're cheating a bit
        pylsd._wlc = 'wl'
        kwargs['fields'] = [pylsd._wlc, pylsd._depth, pylsd._gmean,
                            pylsd._loggf, pylsd._ei]

        # main block
        res = run_lsd(obs_in, mask_in, **kwargs)

        # restoring how it was
        pylsd._wlc = 'wlc'

        # TODO: Add couple of simple checks for some vital parts of the code.


if __name__ == '__main__':
    run_module_suite()
