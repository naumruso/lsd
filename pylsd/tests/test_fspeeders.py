"""
Tests the fspeeders fortran module.
"""
from __future__ import division, print_function, absolute_import

import os
import numpy as np
import cPickle as pickle

from numpy.testing import assert_, assert_equal,\
    assert_almost_equal, assert_array_almost_equal,\
    TestCase, run_module_suite

from pylsd import fspeeders, package_dir


class TestFspeeders(TestCase):
    """
    Test the fortran subroutines from pylsd.fspeeders module.
    Compare the results for a simple numerical problem.
    """
    def setUp(self):
        data_dir = os.path.join(package_dir, 'tests/data/')
        data_file = 'obs.pyb'  # this is a dictionary of lists
        mask_file = 'mask.pyb'  # this is masked array :)

        fin = open(os.path.join(data_dir, data_file), 'rb')
        self.obs = pickle.load(fin)
        fin.close()

        fin = open(os.path.join(data_dir, mask_file), 'rb')
        self.mask = pickle.load(fin)
        fin.close()

        self.cutoff = 0.1
        self.dl = 1
        self.gaps = [[0.0, 3995.0],  # beginning, H lines up to Hepsilon
                     [4072.0, 4132.0],  # Hdelta
                     [4313.0, 4367.0],  # Hgamma
                     [4836.0, 4886.0],  # Hbeta
                     [5257.5, 5339.0],  # CCD gap
                     [5880.0, 6000.0],  # telluric
                     [6270.0, 6330.0],  # telluric
                     [6382.5, 6387.5],  # defect
                     [6460.0, 6590.0],  # telluric, Halpha (6562)
                     [6860.0, 9000.0]]  # telluric, end
        # for the observations
        self.gaps1dpix = np.array(self.gaps, dtype=np.float64).flatten()
        # for the line list
        for i in range(len(self.gaps)):
            self.gaps[i][0] -= self.dl
            self.gaps[i][1] += self.dl
        self.gaps1dlin = np.array(self.gaps, dtype=np.float64).flatten()

        self.out = {}
        self.out['nlin_start'] = 10992
        self.out['nlin_cutoff'] = 8423
        self.out['nlin_gaps'] = 5120
        self.out['npix_start'] = 290816
        self.out['npix_gaps'] = 211095

    def test_data(self):
        # confirm that we have the right data
        assert_equal(self.out['npix_start'], len(self.obs['wl']))
        assert_equal(self.out['nlin_start'], self.mask['wl'].size)

    def test_cutofflande(self):
        # we only check the number of okay lines
        depth = np.array(self.mask['depth'], dtype=np.float64)
        gmean = np.array(self.mask['gmean'], dtype=np.float64)
        idx_cutoff = fspeeders.cutofflande(depth, gmean, self.cutoff, 0)
        idx, = np.where(idx_cutoff >= 0)
        assert_equal(idx.size, self.out['nlin_cutoff'])

    def test_locate_gaps_nlin(self):
        idx, = np.where(fspeeders.cutofflande(self.mask['depth'],
                        self.mask['gmean'], self.cutoff, 0) >= 0)
        wlc = np.asfortranarray(self.mask['wl'][idx])

        # we only check the number of okay lines
        idx_lin, = np.where(fspeeders.locate_gaps(wlc, self.gaps1dlin) >= 0)
        assert_equal(idx_lin.size, self.out['nlin_gaps'])

    def test_locate_gaps_npix(self):
        # we only check the number of okay pixels
        wl = np.array(self.obs['wl'], dtype=np.float64)
        idx_pix, = np.where(fspeeders.locate_gaps(wl, self.gaps1dpix) >= 0)
        assert_equal(idx_pix.size, self.out['npix_gaps'])


if __name__ == '__main__':
    run_module_suite()
